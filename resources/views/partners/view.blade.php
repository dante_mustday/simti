@extends('partners.base')

@section('action-content')

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">View Partner</div>
                <div class="panel-body">
                <form class="form-horizontal" role="form" action="#">    
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') ?: $partner->email }}" readonly>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') ?: $partner->firstname  }}"  readonly>

                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') ?: $partner->lastname  }}"  readonly>

                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">City</label>
                            <div class="col-md-6">
                                <select class="form-control" name="city_id">
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}" {{ ($partner->city_id==$city->id)?"":"" }} >{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') ?: $partner->address  }}"  readonly>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') ?: $partner->phone }}"  readonly>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ route('partners.index') }}" class="btn btn-danger">
                                    Back
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Coverage Area</div>
                <div class="panel-body">
                    <table width="100%" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>City</th>
                                <th>Price Install</th>
                                <th>Price Service</th>
                                <th>Price Maintenance</th>
                                <th>Price Toner</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="content">
                        @foreach($partner->coverage as $row)
                            <tr>
                                <td>{{ $row->city->name }}</td>
                                <td>{{ $row->price_install }}</td>
                                <td>{{ $row->price_service }}</td>
                                <td>{{ $row->price_maintenance }}</td>
                                <td>{{ $row->price_toner }}</td>
                                <td>
                                    <a href="javascript:void(0)" id="update-detail" class="btn btn-warning">update</a>
                                    <a href="javascript:void(0)" id="delete-detail" class="btn btn-danger">x</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
            
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Coverage</div>
                <div class="panel-body">
                    <table width="100%">
                        <thead>
                            <tr>
                                <th>City</th>
                                <th>Price Install</th>
                                <th>Price Service</th>
                                <th>Price Maintenance</th>
                                <th>Price Fee</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                <select class="form-control" name="city_id" id="city_id">
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                                </td>
                                <td>
                                   <input id="price_install" type="number" class="form-control" value="0" name="price_install" autofocus>
                                </td>
                                 <td>
                                   <input id="price_service" type="number" class="form-control" value="0" name="price_service" autofocus>
                                </td>
                                 <td>
                                   <input id="price_maintenance" type="number" class="form-control" value="0" name="price_maintenance" autofocus>
                                </td>
                                 <td>
                                   <input id="price_toner" type="number" class="form-control" value="0" name="price_toner" autofocus>
                                </td>
                               
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <br/>
                                    <button type="button" class="btn btn-primary" onclick="tambah_data()">Add</button>
                                </td>
                               
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
      
</div>
<script type="text/javascript">
function tambah_data(){ 
    var token = '{{ Session::token() }}';
    var service = $('#price_service').val();
    var cityname = $('#city_id option:selected').text();
    var city = $('#city_id option:selected').val();
    var maintenance = $('#price_maintenance').val();
    var install = $('#price_install').val();
    var toner  = $('#price_toner').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type : "POST",
        url : "{{ url('partners/add-coverage') }}/{{ $partner->id }}",  
        data: {city:city,install:install,service:service,maintenance:maintenance,toner:toner,cityname:cityname},
        dataType : 'json',                             
        success : function(data) {
            $('#content').append(data);
            $('#price_service').val("1");
            $('#price_maintenance').val("1");
            $('#price_install').val("1");
            $('#price_toner').val("1");
        }
    });
}      

$(document).ready(function()
{
  
  $("#add-detail").click(function()
  {    
      var city = $("#city_id option:selected").val();
      var name = $("#type option:selected" ).text();
      var qty  = $("#qty").val();
      console.log(type+" "+name+" "+qty);
  new_row="<tr><td><input type='text' name='machine_name[]' value='"+name+"' class='form-control' readonly/><input type='hidden' name='machine_code[]' value='"+type+"' class='form-control' readonly/></td><td><input value='"+qty+"' class='form-control' type='text' name='quantity[]'  readonly/></td><td><a href='#' class='btnDelete btn btn-danger'>x</a></td></tr>";
    $("#content").append(new_row);
    return false;

  });
  $("#content").on('click', '.btnDelete', function () {
    if(confirm("Are you sure, you want to delete this"))
        {
            var tr = $(this).closest('tr');
                tr.css("background-color","#FF3700");
                tr.fadeOut(400, function(){
                    tr.remove();
                });
                return false;
        }
    });
});
</script>
@endsection
