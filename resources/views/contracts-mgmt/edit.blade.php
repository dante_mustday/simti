@extends('contracts-mgmt.base')

@section('action-content')
 <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box  box-info">
            <div class="box-header" style="padding-bottom: 0px;">
                <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Detail Contract</a></li>
              <li><a href="#tab_2" data-toggle="tab">Machine</a></li>
            </ul>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('contract.update', ['id' => $contract->id]) }}">
                    <input type="hidden" name="_method" value="PATCH">
                   {{ csrf_field() }}
                    <div class="box-body">
                                    
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                        <div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                            <label for="customer" class="col-md-2 control-label">Customer</label>

                            <div class="col-md-7">
                                <select class="form-control select2" name="customer" readonly>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}" <?php echo ($customer->id==$contract->customer_id)?"selected":""?>>{{ $customer->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('customer'))  
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('sales') ? ' has-error' : '' }}">
                            <label for="sales" class="col-md-2 control-label">Sales</label>

                            <div class="col-md-7">
                                <select class="form-control select2" name="sales" required>
                                    @foreach ($sales as $sls)--}}
                                        <option value="{{ $sls->id }}" <?php echo ($sls->id==$contract->sales_id)?"selected":""?>>{{ $sls->firstname." ".$sls->lastname }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('sales'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sales') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-2 control-label">Contract Number</label>

                            <div class="col-md-7">
                                <input id="number" type="text" class="form-control" name="number" value="{{ old('number',$contract->contract_number) }}" required>

                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-2 control-label">Contract Date</label>

                            <div class="col-md-7">
                                <input id="date" type="text" class="form-control datepicker" name="date" value="{{ old('date',$contract->contract_date) }}" required>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('po_number') ? ' has-error' : '' }}">
                            <label for="po_number" class="col-md-2 control-label">Customer Po Number</label>

                            <div class="col-md-7">
                                <input id="po_number" type="text" class="form-control" name="po_number" value="{{ old('po_number',$contract->customer_po_number) }}" required>

                                @if ($errors->has('po_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('po_date') ? ' has-error' : '' }}">
                            <label for="po_date" class="col-md-2 control-label">Customer Po Date</label>

                            <div class="col-md-7">
                                <input id="po_date" type="text" class="form-control datepicker" name="po_date" value="{{ old('po_date',$contract->customer_po_date) }}" required>

                                @if ($errors->has('po_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table width="100%" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>Machine</th>
                                <th>Serial Number</th>
                                <th>ASK Number</th>
                                <th>Location</th>
                                <th>Color Meter</th>
                                <th>Mono Meter</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($contract->details as $row)
                            <tr>
                                <td>{{ $row->machine }}</td>
                                <td>{{ $row->serial_number }}</td>
                                <td>{{ $row->ask_code }}</td>
                                <td>{{ $row->location }}</td>
                                <td>{{ $row->color_meter }}</td>
                                <td>{{ $row->mono_meter }}</td>
                                <td>
                                    <a data-toggle="modal" href="#modal-default" 
									data-id="{{ $row->id }}" 
									data-serial="{{ $row->serial_number }}"
									data-ask="{{ $row->ask_code }}"
									data-loc="{{ $row->location }}"
									data-mono="{{ $row->color_meter }}"
									data-color="{{ $row->mono_meter }}"
									id="update-detail" class="btn btn-warning open-update">update</a>
                                    <a href="{{ route('contract.deletemachine', ['contract'=>$contract->id,'id' => $row->id]) }}" id="delete-detail" class="btn btn-danger">delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                  <hr/>
                    <table width="100%">
                        <thead>
                            <tr>
                                <th>Machine Type</th>
                                <th>Quantity</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="content">
                            <td>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                <select class="form-control select2" name="type" id="type">
                                    @foreach ($types as $type)
                                        <option value="{{$type->type_code}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                </td>
                                <td><input type="text" id="qty" name="qty" class="form-control"></td>
                                <td><a href="javascript:void(0)" id="add-detail" class="btn btn-success">+</a></td>
                            </tr>
                        </tfoot>
                    </table>
            </div>
            <!-- /.tab-content -->
                    </div>
                    <!-- /.box-body -->
          </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save fa-btn"></i> Save Campaign
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
<div class="modal fade" id="modal-default" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Machine Detail</h4>
              </div>
              <form class="form-horizontal" role="form" method="POST" action="#">
                    <div class="modal-body"> 

						<div class="form-group">
                            <label for="ask_code" class="col-md-3 control-label">ASK Code</label>

                            <div class="col-md-9">
								<input type="hidden" id="id_detail" name="id_detail">
                                <input id="ask_code" type="text" class="form-control" name="ask_code">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="serial_number" class="col-md-3 control-label">Serial Number</label>

                            <div class="col-md-9">
                                <input id="serial_number" type="text" class="form-control" name="serial_number">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="location" class="col-md-3 control-label">Placement Location</label>

                            <div class="col-md-9"> 
                                <input id="location" type="text" class="form-control" name="location">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="color" class="col-md-3 control-label">Color Meter</label>

                            <div class="col-md-9">
                                <input id="color" type="number" class="form-control" name="color">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="finish" class="col-md-3 control-label">Mono Meter</label>

                            <div class="col-md-9">
                                <input id="mono" type="number" class="form-control" name="mono">
                            </div>
                        </div>

                       
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="tambah_data()">Save</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script type="text/javascript">
$(document).on("click", ".open-update", function () {
     var id = $(this).data('id');
	 var loc = $(this).data('loc');
	 var mono = $(this).data('mono');
	 var color = $(this).data('color');
	 var ask = $(this).data('ask');
	 var serial = $(this).data('serial');
	 console.log(color)
     $("#modal-default").find("#id_detail").val( id );
	 $("#modal-default").find("#location").val( loc );
	 $("#modal-default").find("#mono").val( mono );
	 $("#modal-default").find("#color").val(color);
	 $("#modal-default").find("#ask_code").val(ask);
	 $("#modal-default").find("#serial_number").val(serial);
     // As pointed out in comments, 
     // it is superfluous to have to manually call the modal.
     // $('#addBookDialog').modal('show');
});
$(document).ready(function()
{
  
  $("#add-detail").click(function()
  {    
    var type = $("#type option:selected").val();
      var name = $("#type option:selected" ).text();
      var qty  = $("#qty").val();
      console.log(type+" "+name+" "+qty);
  new_row="<tr><td><input type='text' name='machine_name[]' value='"+name+"' class='form-control' readonly/><input type='hidden' name='machine_code[]' value='"+type+"' class='form-control' readonly/></td><td><input value='"+qty+"' class='form-control' type='text' name='quantity[]'  readonly/></td><td><a href='#' class='btnDelete btn btn-danger'>x</a></td></tr>";
    $("#content").append(new_row);
    return false;

  });
  $("#content").on('click', '.btnDelete', function () {
    if(confirm("Are you sure, you wnat to delete this"))
        {
            var tr = $(this).closest('tr');
                tr.css("background-color","#FF3700");
                tr.fadeOut(400, function(){
                    tr.remove();
                });
                return false;
        }
    });
});
function tambah_data(){ 
    var token = '{{ Session::token() }}';
    var color = $('#color').val();
    var mono = $('#mono').val();
	var id_detail = $('#id_detail').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

		var askcode = $('#ask_code').val();
		var serial = $('#serial_number').val();
		var location = $('#location').val();
		request = {id_detail:id_detail,ask_code:askcode,serial_number:serial,location:location,color:color,mono:mono} ;


    $.ajax({
        type : "POST", 
        url : "{{ url('contract/update-machine') }}",  
        data: request,                                                           
        dataType : 'json',                             
        success : function(data) {
            window.alert(data);
			$('#id_detail').val("");
            $('#color').val("");
            $('#ask_code').val("");
            $('#serial_number').val(""); 
            $('#location').val("");
            $('#mono').val("");
            
        }
    });
    $('#modal-default').modal('hide');
    window.location.reload();
}                   
</script>
        <?php /* ?>
<div class="container" style="width:100%">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('contract.update', ['id' => $contract->id]) }}">
    <div class="row">
        <div class="box  box-info">
            <div class="box-header" style="padding-bottom: 0px;">
                <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Campaign Information</a></li>
              <li><a href="#tab_2" data-toggle="tab">Campaign Time Management</a></li>
              <li><a href="#tab_3" data-toggle="tab">Campaign Budget</a></li>
             
             <li class="pull-right"> <a href="/campaigns" class="btn btn-box-tool" data-toggle="tooltip" title="Back"> <i
                            class="fa fa-times"></i></a></li>
            </ul>
            </div>
            <div class="box-body">
                <form role="form" action="http://ec2-34-209-72-66.us-west-2.compute.amazonaws.com/campaigns" method="POST" enctype='multipart/form-data'>
                    <input type="hidden" name="_token" value="qycVPsoaWmyrfebvsVflOW9ppwNKXfhyJGXg34Go">
                    <div class="box-body">
                                    
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Contract</div>
                        <div class="panel-body">
                    
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       

                        <hr/>
						
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href="{{ route('contract.index') }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Contract Detail</div>
                <div class="panel-body">
                    <table width="100%" class="table table-stripped">
                        <thead>
                            <tr>
                                <th>Machine</th>
                                <th>Serial Number</th>
                                <th>ASKNUMBER</th>
                                <th>Color Meter</th>
                                <th>Mono Meter</th>
                                <!--<th></th>-->
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($contract->details as $row)
                            <tr>
                                <td>{{ $row->machine }}</td>
                                <td>{{ $row->serial_number }}</td>
                                <td>{{ $row->serial_number }}</td>
                                <td>{{ $row->color_meter }}</td>
                                <td>{{ $row->mono_meter }}</td>
                                <!--<td>
                                    <a href="javascript:void(0)" id="update-detail" class="btn btn-warning">update</a>
                                    <a href="javascript:void(0)" id="delete-detail" class="btn btn-danger">x</a>
                                </td>-->
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
            
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Machine</div>
                <div class="panel-body">
                    <table width="100%">
                        <thead>
                            <tr>
                                <th>Machine Type</th>
                                <th>Quantity</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="content">
                            <td>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                <select class="form-control" name="type" id="type">
                                    @foreach ($types as $type)
                                        <option value="{{$type->type_code}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                </td>
                                <td><input type="text" id="qty" name="qty" class="form-control"></td>
                                <td><a href="javascript:void(0)" id="add-detail" class="btn btn-success">+</a></td>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
            </div>
            
        </div>
    </div>

    </form>
</div><?php */ ?>
@endsection
