@extends('contracts-mgmt.base')

@section('action-content')
<div class="container">
    <div class="row">
	<form class="form-horizontal" role="form" method="POST" action="{{ route('contract.store') }}">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Contract</div>
                <div class="panel-body">
                    
                        {{ csrf_field() }}
						<div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                            <label for="customer" class="col-md-3 control-label">Customer</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="customer" required>
								    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('customer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('sales') ? ' has-error' : '' }}">
                            <label for="sales" class="col-md-3 control-label">Sales</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="sales" required>
								    @foreach ($sales as $sls)--}}
                                        <option value="{{ $sls->id }}">{{ $sls->firstname." ".$sls->lastname }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('sales'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sales') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-3 control-label">Contract Number</label>

                            <div class="col-md-9">
                                <input id="number" type="text" class="form-control" name="number" value="{{ old('number') }}" required autofocus>

                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-3 control-label">Contract Date</label>

                            <div class="col-md-9">
                                <input id="date" type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required autofocus>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('po_number') ? ' has-error' : '' }}">
                            <label for="po_number" class="col-md-3 control-label">Customer Po Number</label>

                            <div class="col-md-9">
                                <input id="po_number" type="text" class="form-control" name="po_number" value="{{ old('po_number') }}" required autofocus>

                                @if ($errors->has('po_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('po_date') ? ' has-error' : '' }}">
                            <label for="po_date" class="col-md-3 control-label">Customer Po Date</label>

                            <div class="col-md-9">
                                <input id="po_date" type="text" class="form-control datepicker" name="po_date" value="{{ old('po_date') }}" required autofocus>

                                @if ($errors->has('po_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('po_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                   Save
                                </button>
                            </div>
                        </div>
                  
                </div>
            </div>
        </div>
		<div class="col-md-8" style="padding:0px">
            <div class="panel panel-default">
                <div class="panel-heading">Contract Detail</div>
                <div class="panel-body">
                    <table width="100%">
						<thead>
							<tr>
								<th>Machine Type</th>
								<th>Quantity</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="content">
						
						</tbody>
						<tfoot>
							<tr>
								<td>
                                <select class="form-control select2" name="type" id="type">
                                    @foreach ($types as $type)
                                        <option value="{{$type->type_code}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                </td>
								<td><input type="text" id="qty" name="qty" class="form-control"></td>
								<td><a href="javascript:void(0)" id="add-detail" class="btn btn-success">+</a></td>
							</tr>
						</tfoot>
					</table>
                    
                </div>
            </div>
			
        </div>
	</form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
  
  $("#add-detail").click(function()
  {    
    var type = $("#type option:selected").val();
      var name = $("#type option:selected" ).text();
      var qty  = $("#qty").val();
      //console.log(type+" "+name+" "+qty);
  new_row="<tr><td><input type='text' name='machine_name[]' value='"+name+"' class='form-control' readonly/><input type='hidden' name='machine_code[]' value='"+type+"' class='form-control' readonly/></td><td><input value='"+qty+"' class='form-control' type='text' name='quantity[]'  readonly/></td><td><a href='#' class='btnDelete btn btn-danger'>x</a></td></tr>";
    $("#content").append(new_row);
    return false;

  });
  $("#content").on('click', '.btnDelete', function () {
    if(confirm("Are you sure, you wnat to delete this"))
		{
			var tr = $(this).closest('tr');
				tr.css("background-color","#FF3700");
				tr.fadeOut(400, function(){
					tr.remove();
				});
				return false;
		}
	});
});
</script>
@endsection
