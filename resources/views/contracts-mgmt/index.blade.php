@extends('contracts-mgmt.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of Contract</h3>
        </div>
        <div class="col-sm-4">
          <a class="btn btn-primary" href="{{ route('contract.create') }}">Add new contract</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="{{ route('contract.search') }}">
         {{ csrf_field() }}
         @component('layouts.search', ['title' => 'Search'])
          @component('layouts.two-cols-search-row', ['items' => ['Contract Number'], 
          'oldVals' => [isset($searchingVals) ? $searchingVals['contract_number'] : '']])
          @endcomponent
        @endcomponent
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                
                <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="code: activate to sort column ascending">Contract Number</th>
				<th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="contract: activate to sort column ascending">Customer</th>			
				<th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="contract: activate to sort column ascending">Contract Date</th>			
				<th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="contract: activate to sort column ascending">Customer PO</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            {{$contracts}}
            @foreach ($contracts as $contract)
                <tr role="row" class="odd">
                  <td>{{ $contract->contract_number }}</td>
				  <td>{{ $contract->customer->name }}</td>
				  <td>{{ $contract->contract_date }}</td>
                  <td>{{ $contract->customer_po_number }}</td>
                  <td>
                    <form class="row" method="POST" action="{{ route('contract.destroy', ['id' => $contract->id]) }}" onsubmit = "return confirm('Are you sure?')">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="{{ route('contract.edit', ['id' => $contract->id]) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                       Edit
                        </a>
                        <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          Delete
                        </button>
                    </form>
                  </td>
              </tr>
            @endforeach
            </tbody>
            <!--<tfoot>
              <tr>
                <th width="20%" rowspan="1" colspan="1">Code</th>
                <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="contract: activate to sort column ascending">Machine Name</th>
                <th rowspan="1" colspan="2">Action</th>
              </tr>
            </tfoot>-->
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
          <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{count($contracts)}} of {{count($contracts)}} entries</div>
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            {{ $contracts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection