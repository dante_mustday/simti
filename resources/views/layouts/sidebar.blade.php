  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name}}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> 
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{ url('dashboard') }}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>

        @if( Auth::user()->roles == 4 || Auth::user()->roles == 0)
		    <li><a href="{{ url('contract') }}"><i class="fa fa-file-archive-o"></i> <span>Contract Management</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-ticket"></i> <span>Ticketing Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('ticketing/create') }}">Create New</a></li>
            <li><a href="{{ url('ticketing') }}">On Progress</a></li>
            <li><a href="{{ url('ticketing-emergency') }}">Emergency</a></li>
            <li><a href="{{ url('ticketing-finish') }}">Finish</a></li>
          </ul>
        </li>
        <li><a href="{{ url('employee-management') }}"><i class="fa fa-child"></i> <span>Employee Management</span></a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-male"></i> <span>Relations Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('partners') }}">Partners</a></li>
            <li><a href="{{ url('customer') }}">Customer</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-cogs"></i> <span>System Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!--
            <li><a href="{{ url('system-management/department') }}">Department</a></li>
            <li><a href="{{ url('system-management/country') }}">Country</a></li>
            <li><a href="{{ url('system-management/state') }}">State</a></li>
            <li><a href="{{ url('system-management/report') }}">Report</a></li>
            -->
            <li><a href="{{ url('system-management/division') }}">Division</a></li>
            <li><a href="{{ url('system-management/city') }}">City</a></li>
			<!--<li><a href="{{ url('system-management/districts') }}">Districts</a></li>-->
			<li><a href="{{ url('system-management/machine') }}">Machine</a></li>
            <li><a href="{{ url('system-management/service-category') }}">Action Category</a></li>
			<li><a href="{{ url('system-management/service') }}">Action Description</a></li>
            <li><a href="{{ url('system-management/sla') }}">Service Level Agreement</a></li>
            
          </ul>
        </li>
        <li><a href="{{ route('user-management.index') }}"><i class="fa fa-users"></i> <span>User management</span></a></li>
		@elseif(Auth::user()->roles == 5 || Auth::user()->roles == 6 || Auth::user()->roles == 3))
		<li class="treeview">
          <a href="#"><i class="fa fa-ticket"></i> <span>My Ticket</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
			@if(Auth::user()->roles == 6))
			<li><a href="{{ url('ticketing/create') }}">Create New</a></li>
			@endif
            <li><a href="{{ url('ticketing') }}">On Progress</a></li>
            <li><a href="{{ url('ticketing-emergency') }}">Emergency</a></li>
            <li><a href="{{ url('ticketing-finish') }}">Finish</a></li>
          </ul>
        </li>
        @else
          
        @endif
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>