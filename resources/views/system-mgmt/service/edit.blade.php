@extends('system-mgmt.service.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Service</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('service.update', ['id' => $service->id]) }}">
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <div class="form-group">
                            <label for="category_id" class="col-md-4 control-label">Category</label>

                            <div class="col-md-8">
                                <select class="form-control" name="category_id" id="category_id" required>
                                    <option value="1" <?php echo ($service->category_id=='1')?"selected":"" ;?> >Install</option>
                                    <option value="2" <?php echo ($service->category_id=='2')?"selected":"" ;?> >Service</option>
                                    <option value="3" <?php echo ($service->category_id=='3')?"selected":"" ;?> >Maintenance</option>
                                    <option value="4" <?php echo ($service->category_id=='4')?"selected":"" ;?> >Toner / Ink</option>
									<option value="5" <?php echo ($service->category_id=='5')?"selected":"" ;?> >Others</option>
									<option value="6" <?php echo ($service->category_id=='6')?"selected":"" ;?> >Demo Unit</option>
									<option value="7" <?php echo ($service->category_id=='7')?"selected":"" ;?> >Tarik Meter</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_name') ? ' has-error' : '' }}">
                            <label for="service_name" class="col-md-4 control-label"> Service Name</label>

                            <div class="col-md-6">
                                <input id="service_name" type="text" class="form-control" name="service_name" value="{{ old('service_name') ?: $service->service_name }}" required autofocus>

                                @if ($errors->has('service_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('service_code') ? ' has-error' : '' }}">
                            <label for="service_code" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="service_code" type="text" class="form-control" name="service_code" value="{{ old('service_code') ?: $service->service_code }}" required autofocus>

                                @if ($errors->has('service_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_code') }}</strong>
                                    </span>
                                @endif
                            </div> 
                        </div>
						  <div class="form-group">
                            <label for="need_approval" class="col-md-4 control-label">Need Approval ??</label>

                            <div class="col-md-8"> 
                                <select class="form-control" name="need_approval" id="need_approval" required>
                                    <option value="0" <?php echo ($service->need_approval=='0')?"selected":"" ;?> >No</option>
                                    <option value="1" <?php echo ($service->need_approval=='1')?"selected":"" ;?> >Yes</option>
                                </select>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
