@extends('system-mgmt.service.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add new service</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('service.store') }}">
                        {{ csrf_field() }}
                         <div class="form-group">
                            <label for="category_id" class="col-md-4 control-label">Category</label>

                            <div class="col-md-8">
                                <select class="form-control" name="category_id" id="category_id" required>
                                    <option value="1">Install</option>
                                    <option value="2">Service</option>
                                    <option value="3">Maintenance</option>
                                    <option value="4">Toner / Ink</option>
									<option value="5">Others</option>
									<option value="6">Demo Unit</option>
									<option value="7">Tarik Meter</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('service_name') ? ' has-error' : '' }}">
                            <label for="service_name" class="col-md-4 control-label">Service Name</label>

                            <div class="col-md-6">
                                <input id="service_name" type="text" class="form-control" name="service_name" value="{{ old('service_name') }}" required autofocus>

                                @if ($errors->has('service_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('service_code') ? ' has-error' : '' }}">
                            <label for="service_code" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="service_code" type="text" class="form-control" name="service_code" value="{{ old('service_code') }}" required autofocus>

                                @if ($errors->has('service_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('service_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="need_approval" class="col-md-4 control-label">Need Approval ??</label>

                            <div class="col-md-8">
                                <select class="form-control" name="need_approval" id="need_approval" required>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
						
                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
