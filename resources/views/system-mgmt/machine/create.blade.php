@extends('system-mgmt.machine.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add new machine</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('machine.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label"> Machine Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('type_code') ? ' has-error' : '' }}">
                            <label for="type_code" class="col-md-4 control-label">Code</label>

                            <div class="col-md-6">
                                <input id="type_code" type="text" class="form-control" name="type_code" value="{{ old('type_code') }}" required autofocus>

                                @if ($errors->has('type_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
