@extends('ticketing-mgmt.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of Ticket</h3>
        </div>
        <!--<div class="col-sm-4">
          <a class="btn btn-primary" href="{{ route('ticketing.create') }}">Add new Ticket</a>
        </div>-->
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      <form method="POST" action="{{ route('ticketing.search') }}">
         {{ csrf_field() }}
         @component('layouts.search', ['title' => 'Search'])
          @component('layouts.two-cols-search-row', ['items' => ['Name'], 
          'oldVals' => [isset($searchingVals) ? $searchingVals['name'] : '']])
          @endcomponent
        @endcomponent
      </form>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="20%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Customer</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="code: activate to sort column ascending">Ticket Number</th>
				        <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Ticket Date</th>
<th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Support Handle</th>								
				        <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Call Name</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Level</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Ticket: activate to sort column ascending">Limit</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            
            @foreach ($ticketings as $ticket)
                <tr role="row" class="odd">
                  <td>{{ $ticket->customer->name }}</td>
                  <td>{{ $ticket->ticketing_code }}</td>
				  <td>{{ $ticket->open_ticket_date }}</td>
			      <td>{{ @$ticket->technician->firstname." ".@$ticket->technician->lastname }}</td>
                  <td>{{ $ticket->call_name }}</td>
                  <td>{{ @$ticket->sla->name }}</td>
                  <td>{{ $ticket->limit_ticket_date }}</td>
                  <td>
                    <!--<form class="row" method="POST" action="{{ route('ticketing.destroy', ['id' => $ticket->id]) }}" onsubmit = "return confirm('Are you sure?')">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                        <!--<a href="{{ route('ticketing.edit', ['id' => $ticket->id]) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        Update Data
                        </a>--> 
                        @if( Auth::user()->roles == 4 || Auth::user()->roles == 0)
						<a href="{{ route('ticketing.edit', ['id' => $ticket->id]) }}" class="btn btn-success btn-margin">
							Change Techinician
						</a>
                        @endif
						
                        <a href="{{ route('ticketing.add', ['id' => $ticket->id]) }}" class="btn btn-danger btn-margin">
						@if( $ticket->status==0)
							Add Action 
						@else
							Detail
						@endif
                        </a>
                        <!--<button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          Delete
                        </button>
                    </form>-->
                  </td>
              </tr>
            @endforeach
            </tbody>

          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
          <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{count($ticketings)}} of {{count($ticketings)}} entries</div>
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            {{ $ticketings->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection