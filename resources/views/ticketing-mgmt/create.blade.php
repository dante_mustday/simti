@extends('ticketing-mgmt.base')

@section('action-content')
<div class="container">
    <div class="row">
	<form class="form-horizontal" role="form" method="POST" action="{{ route('ticketing.store') }}">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ticket</div>
                <div class="panel-body">

                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-3 control-label">Type</label>

                            <div class="col-md-9">
                                <select class="form-control" name="type" required>
                                    <option value="0">Ticketing</option>
                                    <option value="1">Work Order</option>
                                </select>

                                @if($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                            <label for="city_id" class="col-md-3 control-label">City</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="city_id" id="city_id" onchange="get_technicianbycity(this.value)" >
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('is_internal') ? ' has-error' : '' }}">
                            <label for="is_internal" class="col-md-3 control-label">Internal ??</label>

                            <div class="col-md-9">
                                <select class="form-control" name="is_internal" id="is_internal" onchange="get_technician(this.value)" required>
                                    <option value="0">Yes</option>
                                    <option value="1">No</option>
                                </select>

                                @if($errors->has('is_internal'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('is_internal') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('technician') ? ' has-error' : '' }}">
                            <label for="technician" class="col-md-3 control-label">Technician</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="technician" id="technician" required>
                                    <option value="">Select Technician</option>
                                    @foreach ($technicians as $technician)
                                        <option value="{{ $technician->id }}">{{ $technician->firstname }} {{ ($technician->lastname!='-')?$technician->lastname:'' }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('technician'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('technician') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                            <label for="customer" class="col-md-3 control-label">Customer</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="customer" id="customer" onchange="get_contract(this.value)" required>
                                    <option value="">Select Customer</option>
								    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('customer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('contract') ? ' has-error' : '' }}">
                            <label for="contract" class="col-md-3 control-label">Contract</label>

                            <div class="col-md-9">
                                <select class="form-control select2" id="contract" name="contract" onchange="get_machine(this.value)" required>
                                    <option value="">Select Contract Number</option>
                                </select>

                                @if ($errors->has('contract'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contract') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('machine') ? ' has-error' : '' }}">
                            <label for="machine" class="col-md-3 control-label">Machine</label>

                            <div class="col-md-9">
                                <select class="form-control select2" id="machine" name="machine" required>
                                    <option value="">Select Machine</option>
                                </select>

                                @if ($errors->has('machine'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('machine') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sla_id') ? ' has-error' : '' }}">
                            <label for="sla_id" class="col-md-3 control-label">SLA</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="sla_id" id="sla_id">
                                    @foreach ($slas as $sla)
                                        <option value="{{$sla->id}}">{{$sla->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('sla_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sla_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-3 control-label">Action Category</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="category" id="category" required>
								    @foreach ($categorys as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-3 control-label">Machine Location</label>

                            <div class="col-md-9">
                                <input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" required autofocus>

                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-3 control-label">Call Date</label>

                            <div class="col-md-9">
                                <input id="date" type="text" class="form-control datepicker" name="date" value="{{ old('date') }}" required autofocus>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('call_time') ? ' has-error' : '' }}">
                            <label for="call_name" class="col-md-3 control-label">Call Time</label>

                            <div class="col-md-9">
                                <input id="call_time" type="time" class="form-control" name="call_time" value="{{ old('call_time') }}" required autofocus>

                                @if ($errors->has('call_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('call_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('call_name') ? ' has-error' : '' }}">
                            <label for="call_name" class="col-md-3 control-label">Call Name</label>

                            <div class="col-md-9">
                                <input id="call_name" type="text" class="form-control" name="call_name" value="{{ old('call_name') }}" required autofocus>

                                @if ($errors->has('call_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('call_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('call_phone') ? ' has-error' : '' }}">
                            <label for="call_phone" class="col-md-3 control-label">Call Phone</label>

                            <div class="col-md-9">
                                <input id="call_phone" type="text" class="form-control" name="call_phone" value="{{ old('call_phone') }}" required autofocus>

                                @if ($errors->has('call_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('call_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                   Save
                                </button>
                            </div>
                        </div>

                </div>
            </div>
        </div>
	</form>
    </div>
</div>
<script type="text/javascript">
    function get_contract(cust){
        $('#contract').load('{{ url("/ajax/get_contract_by_customer") }}/'+cust);
    }


    function get_machine(id){
        $('#machine').load('{{ url("/ajax/get_machine_by_contract") }}/'+id);
    }

	function get_technician(id){
		var city = document.getElementById('city_id').value;
        $('#technician').load('{{ url("/ajax/get_technician_by_ticket_type") }}/'+id+'/'+city);
    }

	function get_technicianbycity(city){
		var id = document.getElementById('is_internal').value;
        $('#technician').load('{{ url("/ajax/get_technician_by_ticket_type") }}/'+id+'/'+city);
    }

$(document).ready(function()
{

});
</script>
@endsection
