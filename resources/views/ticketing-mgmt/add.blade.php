@extends('ticketing-mgmt.base')

@section('action-content') 
<section class="content">
<div class="box  box-info">
	<div class="box-header" style="padding-bottom: 0px;">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Detail Ticket</a></li>
            <li><a href="#tab_2" data-toggle="tab">Action</a></li>
			<li><a href="#tab_3" data-toggle="tab">Request Sparepart</a></li>
        </ul>
    </div>
    <div class="box-body">
	<form class="form-horizontal" role="form" action="#">
		<div class="tab-content">
            <div class="tab-pane active" id="tab_1">
				@if($ticketing->status==0)
					@if(count($ticketing->details)>0)
						<a href="{{ url('ticketing/closed') }}/{{ $ticketing->id }}" class='btn btn-warning pull-right'>Closed Ticket</a>
					@endif
				@endif
				@if( Auth::user()->roles == 4 ||  Auth::user()->roles == 5 ||Auth::user()->roles == 0)
					@if($ticketing->status==1)
					<a href="{{ url('ticketing/decline-closed') }}/{{ $ticketing->id }}" class='btn btn-warning pull-right'>Decline Closed</a>
					<a href="{{ url('ticketing/approve-closed') }}/{{ $ticketing->id }}" class='btn btn-success pull-right'>Approved Closed</a>
					@endif
				@endif
		
                <div class="panel-body">
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-3 control-label">Ticket Number : </label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->ticketing_code }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-3 control-label">Type : </label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ ($ticketing->is_wo==0)?"Ticketing":"Work Order" }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('is_internal') ? ' has-error' : '' }}">
                            <label for="is_internal" class="col-md-3 control-label">Internal ?? :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ ($ticketing->is_wo==0)?"Yes":"No (Assigned To Partner)" }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('technician') ? ' has-error' : '' }}">
                            <label for="technician" class="col-md-3 control-label">Technician :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ @$ticketing->technician->firstname." ".@$ticketing->technician->lastname }}
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                            <label for="customer" class="col-md-3 control-label">Customer :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->customer->name }}
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('contract') ? ' has-error' : '' }}">
                            <label for="contract" class="col-md-3 control-label">Contract :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->contract->contract_number }}
                            </div>
                        </div>
						
                        <div class="form-group{{ $errors->has('machine') ? ' has-error' : '' }}">
                            <label for="machine" class="col-md-3 control-label">Machine :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ @$ticketing->machine }}
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-3 control-label">City :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->city->name }}  
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-3 control-label">Action Category :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                <!--{{ $ticketing->machine_location }}-->
                            </div>
                        </div>
                         

                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-3 control-label">Machine Location :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->machine_location }}
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="date" class="col-md-3 control-label">Call Date :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->open_ticket_date }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('call_time') ? ' has-error' : '' }}">
                            <label for="call_name" class="col-md-3 control-label">Call Time :</label>

                           <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->call_time }}
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('call_name') ? ' has-error' : '' }}">
                            <label for="call_name" class="col-md-3 control-label">Call Name :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->call_name }}
                            </div>
                        </div>
						
						<div class="form-group{{ $errors->has('call_phone') ? ' has-error' : '' }}">
                            <label for="call_phone" class="col-md-3 control-label">Call Phone :</label>

                            <div class="col-md-9" style="padding-top: 7px;">
                                {{ $ticketing->call_phone }}
                            </div>
                        </div>
                  
                </div>
            </div>
			<div class="tab-pane" id="tab_2">
                <table class="table table-stripped" width="100%">
                    <thead>
                        <tr>
                            <th>Visit Date</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Service</th>
                            <th>Description</th>
                            <th>Result</th>
                            <th></th>
                        </tr>
                    </thead>
             
                    <tbody id="contentmaterial">
                        @foreach($ticketing->details as $row)
                            <tr>
                                <td>{{ $row->visit_date }}</td>
                                <td>{{ $row->start }}</td>
                                <td>{{ $row->finish }}</td>
                                <td>{{ $row->action }}</td>
                                <td>{{ $row->description }}</td>
                                <td>{{ $row->result }}</td>
                                <td><a href='javascript:void(0)' class='btn tbn-danger' onclick='delete_action({{ $row->id }})'>Delete</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if($ticketing->status==0)
                        
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
                            Add Action
                        </button>
                        
                    @endif
           
            </div>
			<div class="tab-pane" id="tab_3">
						<table class="table table-stripped" width="100%">
							<thead>
								<tr>
									<th>Sparepart Number</th>
									<th>Sparepart Name</th>
									<th>Reason</th>
									<th>Status</th>
									<th>Supervisor</th>
									<th></th>
								</tr>
							</thead>
				 
							<tbody id="contentrequest">
							@foreach ($ticketing->request as $row)
								<tr>
									<td>{{ $row->sparepart_code }}</td>
									<td>{{ $row->sparepart_name }}</td>
									<td>{{ $row->reason }}</td>
									<td>{{ $row->status }}</td>
									<td>{{ $row->supervisor_id }}</td>
									<td>
									@if( Auth::user()->roles == 4 ||  Auth::user()->roles == 5 || Auth::user()->roles == 0)
										@if($row->is_approved==0)
										<a href="{{ url('ticketing/approve-request') }}/{{ $row->id }}/{{ $ticketing->id }}" class='btn btn-info'>Approve</a>
										<a href="{{ url('ticketing/reject-request') }}/{{ $row->id }}/{{ $ticketing->id }}" class='btn btn-danger'>Reject</a>
										@endif
									@endif
									@if($row->is_approved==1)
										<button type="button" class="btn btn-info modal-request" data-id="{{ $row->id }}" data-toggle="modal" data-target="#modal-default3">
											Update Data Sparepart
										</button>
										<a href="{{ url('ticketing/cancel-request') }}/{{ $row->id }}/{{ $ticketing->id }}" class='btn btn-danger'>Cancel</a>
										@endif
									</td>
									
								</tr>
							@endforeach
							</tbody>
						</table>
						@if($ticketing->status==0)
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-default2">
								Request Sparepart
							</button>
						@endif
			</div>
		</div>
	</form>
	</div>
</div>
</section>
<div class="modal fade" id="modal-default" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Enginer Action</h4>
              </div>
              <form class="form-horizontal" role="form" method="POST" action="#">
                    <div class="modal-body"> 

                        <div class="form-group">
                            <label for="date" class="col-md-3 control-label">Visit Date</label>

                            <div class="col-md-9">
								<input type="hidden" name="is_wo" id="is_wo" value="{{ $ticketing->is_wo }}">
								<input type="hidden" name="id_detail" id="id_detail" value="{{ $ticketing->id_detail }}">
                                <input id="date" type="text" class="form-control datepicker" name="date" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="start" class="col-md-3 control-label">Start</label>

                            <div class="col-md-9">
                                <input id="start" type="time" class="form-control" name="start" value="{{ old('start') }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="finish" class="col-md-3 control-label">Finish</label>

                            <div class="col-md-9">
                                <input id="finish" type="time" class="form-control" name="finish" value="{{ old('finish') }}" required>
                            </div>
                        </div>
						@if($ticketing->is_wo == 1) 
						<div class="form-group">
                            <label for="ask_code" class="col-md-3 control-label">ASK Code</label>

                            <div class="col-md-9">
                                <input id="ask_code" type="text" class="form-control" name="ask_code">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="serial_number" class="col-md-3 control-label">Serial Number</label>

                            <div class="col-md-9">
                                <input id="serial_number" type="text" class="form-control" name="serial_number">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="location" class="col-md-3 control-label">Placement Location</label>

                            <div class="col-md-9">
                                <input id="location" type="text" class="form-control" name="location">
                            </div>
                        </div>
						@endif
                        <div class="form-group">
                            <label for="color" class="col-md-3 control-label">Color Meter</label>

                            <div class="col-md-9">
                                <input id="color" type="number" class="form-control" name="color" onblur="calculate()">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="finish" class="col-md-3 control-label">Mono Meter</label>

                            <div class="col-md-9">
                                <input id="mono" type="number" class="form-control" name="mono" onblur="calculate()">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="finish" class="col-md-3 control-label">Total Meter</label>

                            <div class="col-md-9">
                                <input id="total" type="number" class="form-control" name="total" readonly>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="service" class="col-md-3 control-label">Action</label>

                            <div class="col-md-9">
                                <select class="form-control select2" style="width:100%" name="service" id="service" required>
                                    @foreach($services as $service)
                                        <option value="{{ $service->service_code }}">{{ $service->service_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="description" class="col-md-3 control-label">Description</label>

                            <div class="col-md-9">
                                <textarea name="description" id="description" class="form-control"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="result" class="col-md-3 control-label">Result</label>

                            <div class="col-md-9">
                                <textarea name="result" id="result" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="tambah_data()">Save</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal-default2" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Request Change Sparepart</h4>
              </div>
              <form class="form-horizontal" role="form" method="POST" action="#">
                    <div class="modal-body">  
                        
                        <div class="form-group">
                            <label for="reason" class="col-md-3 control-label">Reason For Change</label>

                            <div class="col-md-9">
                                <textarea name="reason" id="reason" class="form-control"></textarea>
                            </div>
                        </div>

                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="tambah_data2()">Save</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<div class="modal fade" id="modal-default3" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Update Data Sparepart</h4>
              </div>
              <form class="form-horizontal" role="form" method="POST" action="#">
                    <div class="modal-body">  
                        
                        <div class="form-group">
                            <label for="sparepart_code" class="col-md-3 control-label">Sparepart Code</label>

                            <div class="col-md-9">
								<input id="id_request" type="hidden" class="form-control" name="id_request">
                                <input id="sparepart_code" type="text" class="form-control" name="sparepart_code">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sparepart_name" class="col-md-3 control-label">Sparepart Name</label>

                            <div class="col-md-9">
                                <input id="sparepart_name" type="text" class="form-control" name="sparepart_name">
                            </div>
                        </div>
                        

                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="update_sparepart()">Save</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script type="text/javascript">
function calculate(){  
var mono = document.getElementById('mono').value
var color = document.getElementById('color').value
document.getElementById('total').value = parseFloat(mono) + parseFloat(color);
}
function tambah_data(){ 
    var token = '{{ Session::token() }}';
    var service = $('#service option:selected').val();
    var action = $('#service option:selected').text();
    var color = $('#color').val();
    var description = $('#description').val();
    var result  = $('#result').val();
    var mono = $('#mono').val();
    var total = $('#total').val();
    var date = $('#date').val();
    var start = $('#start').val();
    var finish = $('#finish').val();
	
	var is_wo = $('#is_wo').val();
	var id_detail = $('#id_detail').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	if(is_wo==0){
		request = {service:service,is_wo:is_wo,id_detail:id_detail,color:color,description:description,result:result,mono:mono,total:total,date:date,start:start,finish:finish,action:action} ;
	}else{
		var askcode = $('#ask_code').val();
		var serial = $('#serial_number').val();
		var location = $('#location').val();
		request = {service:service,id_detail:id_detail,ask_code:askcode,serial_number:serial,location:location,is_wo:is_wo,color:color,description:description,result:result,mono:mono,total:total,date:date,start:start,finish:finish,action:action} ;
	}

    $.ajax({
        type : "POST",
        url : "{{ url('ticketing/add-action') }}/{{ $ticketing->ticketing_code }}",  
        data: request,                                                           
        dataType : 'json',                             
        success : function(data) {
            $('#contentmaterial').append(data);
            $('#description').val("");
            $('#cmb_start').val("");
            $('#service').val("1");
            $('#color').val("");
            $('#result').val("");
            $('#total').val(""); 
            $('#date').val("");
            $('#mono').val(""); 
            $('#finish').val("");
            
        }
    });
    $('#modal-default').modal('hide');
    window.location.reload();
}                                 
function tambah_data2(){     
            var reason = $('#reason').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : "POST",
                url : "{{ url('ticketing/add-request') }}/{{ $ticketing->ticketing_code }}",  
                data: {reason:reason},                                                           
                dataType : 'json',                             
                success : function(data) {
                    $('#contentrequest').append(data);
                    $('#reason').val("");
                    
                }
            });

            $('#modal-default2').modal('hide');
            window.location.reload();
      }
$(document).on("click", ".modal-request", function () {
     var id = $(this).data('id');
	 console.log(id)
     $("#modal-default3").find("#id_request").val( id );
});	  
function update_sparepart(){     
            var code = $('#sparepart_code').val();
			var name = $('#sparepart_name').val();
			var id = $('#id_request').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
			console.log(code+" "+name)
            $.ajax({
                type : "POST",
                url : "{{ url('ticketing/update-request') }}/"+id,  
                data: {code:code,name:name},                                                           
                dataType : 'json',                             
                success : function(data) {
                    $('#sparepart_code').val("");
                    $('#sparepart_name').val("");
                }
            });

            $('#modal-default3').modal('hide');
            window.location.reload();
}
/*
$.ajax({
        url: 'register',
        type: 'POST',
        data: {variable1: variable1, variable2: variable2, _token: token},,
        success: function(msg){
            alert(msg);
        }
        contentType: false,
        processData: false

    });*/
</script>
@endsection
