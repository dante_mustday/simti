@extends('layouts.realtime')

@section('content')
<div class="containers">
    <div class="rows">
        <div class="col-md-3">
			<div class="div-content">
			<h5 class="text-info">
				On Progress
			</h5>
			<ul id="onprogress">
				
			</ul>
			</div>
		</div>
		<div class="col-md-3">
            <div class="div-content">
			<h5 class="text-danger">
				Emergency
			</h5>
			<ul id="emergency">
				
			</ul>
			</div>
		</div>
		<div class="col-md-3">
            <div class="div-content">
			<h5 class="text-warning">
				Deadline
			</h5>
			<ul id="deadline">
				
			</ul>
			</div>
		</div>
		<div class="col-md-3">
            <div class="div-content">
			<h5 class="text-success">
				Finish
			</h5>
			<ul id="finish">
				
			</ul>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready( function(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
                            + momentNow.format('dddd')
                             .substring(0,3).toUpperCase());
        $('#time-part').html(momentNow.format('A hh:mm:ss'));
    }, 100);

	var heightwindow = $(document).height()-100;
	console.log(heightwindow)
	$('.div-content').css('height', heightwindow+'px');
	$.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
	  setInterval(function() {
		$('#onprogress').load("{{ url('ajax/on-progress') }}");
	  }, 6000); // the "3000" 
	  setInterval(function() {
		$('#emergency').load("{{ url('ajax/emergencys') }}");
	  }, 7500); // the "3000" 
	  setInterval(function() {
		$('#deadline').load("{{ url('ajax/over-deadline') }}");
	  }, 10000); // the "3000" 
	  setInterval(function() {
		$('#finish').load("{{ url('ajax/finish') }}");
	  }, 9000); // the "3000" 
});
</script>
@endsection
