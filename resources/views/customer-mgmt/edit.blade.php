@extends('customer-mgmt.base')

@section('action-content')
<div class="containers">
    <div class="rows">
		<form class="form-horizontal" role="form" method="POST" action="{{ route('customer.update', ['id' => $customer->id]) }}">
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">Update</div>
                <div class="panel-body">
                    
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                            <label for="city_id" class="col-md-3 control-label">City</label>

                            <div class="col-md-9">
                                <select class="form-control select2" name="city_id" id="city_id">
                                    @foreach ($cities as $city)
                                        <option value="{{$city->id}}" <?php echo ($city->id==$customer->city_id)?"selected":""  ;?>>{{$city->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-3 control-label">#ID</label>

                            <div class="col-md-9">
                                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') ?: $customer->code  }}" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-3 control-label">Name</label>

                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') ?: $customer->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-3 control-label">Address</label>

                            <div class="col-md-9">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') ?: $customer->address}}" required autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-3 control-label">Email</label>

                            <div class="col-md-9">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') ?: $customer->email}}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-3 control-label">Phone</label>

                            <div class="col-md-9">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') ?: $customer->phone }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pic_name') ? ' has-error' : '' }}">
                            <label for="pic_name" class="col-md-3 control-label">PIC Name</label>

                            <div class="col-md-9">
                                <input id="pic_name" type="text" class="form-control" name="pic_name" value="{{ old('pic_name') ?: $customer->pic_name}}" required autofocus>

                                @if ($errors->has('pic_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pic_phone') ? ' has-error' : '' }}">
                            <label for="pic_phone" class="col-md-3 control-label">PIC phone</label>

                            <div class="col-md-9">
                                <input id="pic_phone" type="text" class="form-control" name="pic_phone" value="{{ old('pic_phone') ?: $customer->pic_phone}}" required autofocus>

                                @if ($errors->has('pic_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pic_email') ? ' has-error' : '' }}">
                            <label for="pic_email" class="col-md-3 control-label">PIC Email</label>

                            <div class="col-md-9">
                                <input id="pic_email" type="text" class="form-control" name="pic_email" value="{{ old('pic_email') ?: $customer->pic_email}}" required autofocus>

                                @if ($errors->has('pic_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pic_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
		<div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">SLA</div>
                <div class="panel-body">
					<table width="100%">
                        <thead>
                            <tr>
                                <th>Priority</th>
                                <th>Maximum (hours)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="content">
							@foreach($customer->sla as $row)
                            <tr>
								<td>{{ $row->name }}</td>
								<td>{{ $row->maximum }}</td>
							</tr>
							@endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
								<select class="form-control" id="priority" name="priority">
                                    @foreach ($sla as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
								<!--<input type="text" id="priority" name="priority" class="form-control">-->
                                </td>
                                <td><input type="number" id="maximum" name="maximum" class="form-control"></td>
                                <td><a href="javascript:void(0)" id="add-detail" class="btn btn-success">+</a></td>
                            </tr>
                        </tfoot>
                    </table>
				</div>
			</div>
		</div>
		</form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
  
  $("#add-detail").click(function()
  {    
      var type = $("#priority option:selected").val();
      var name = $("#priority option:selected" ).text();
      //var name = $("#priority" ).val();
      var qty  = $("#maximum").val();
  new_row="<tr><td><input type='text' name='priority[]' value='"+name+"' class='form-control' readonly/></td><td><input value='"+qty+"' class='form-control' type='text' name='maximum[]'  readonly/></td><td><a href='#' class='btnDelete btn btn-danger'>x</a></td></tr>";
    $("#content").append(new_row);
    return false;

  });
  $("#content").on('click', '.btnDelete', function () {
    if(confirm("Are you sure, you wnat to delete this"))
        {
            var tr = $(this).closest('tr');
                tr.css("background-color","#FF3700");
                tr.fadeOut(400, function(){
                    tr.remove();
                });
                return false;
        }
    });
});
</script>
@endsection
