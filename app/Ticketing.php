<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketing extends Model
{
    protected $table = 'ticketing';
	
    protected $guarded = [];

    public function details()
    {
        return $this->hasMany(TicketingDetail::class, 'ticketing_code', 'ticketing_code');
    }
	
	public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function contract()
    {
        return $this->belongsTo(Contracts::class, 'contract_id');
    }

    public function technician()
    {
        return $this->belongsTo(Employee::class, 'technician_id');
    }

    public function machine()
    {
        return $this->belongsTo(ContractsDetail::class, 'machine_code');
    }
	
	public function sla()
    {
        return $this->belongsTo(SlaCustomer::class, 'sla_id');
    }

    public function slamaster()
    {
        return $this->belongsTo(Sla::class, 'sla_id');
    }

    public function request()
    {
        return $this->hasMany(TicketingRequest::class, 'ticketing_code', 'ticketing_code');
    }
	
	public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
