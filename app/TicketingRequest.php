<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketingRequest extends Model
{
   protected $table = 'ticketing_request_sparepart';
	
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Ticketing::class, 'ticketing_code', 'ticketing_code');
    }


}
