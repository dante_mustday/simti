<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sla extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sla';


    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

}
