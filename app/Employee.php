<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
	protected $table = 'employees';
	
     use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at']; 
	
	public function City()
    {
        return $this->hasOne(City::class, 'city_id');
    }
	
	public function State()
    {
        return $this->hasOne(State::class, 'state_id');
    }
	
	public function Division()
    {
        return $this->hasOne(Division::class, 'division_id');
    }
	
	public function Departement()
    {
        return $this->hasOne(Departement::class, 'departement_id');
    }

    public function User()
    {
        return $this->hasOne(User::class, 'employee_id');
    }
}
