<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'districts';


    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];
	
	public function City()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
	
}
