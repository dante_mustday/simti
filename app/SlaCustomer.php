<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlaCustomer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sla_customer';

	public function customer()
    {
        return $this->hasOne(Customer::class,'customer_id');
    }
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

}
