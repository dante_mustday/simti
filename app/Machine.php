<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Machine extends Model
{
	
	protected $table = 'machine';
	
     use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at']; 
	
	public function Ticketing()
    {
        return $this->hasMany(Ticketing::class, 'machine_code','code');
    }
	
	public function Contract()
    {
        return $this->hasMany(ContractsDetail::class, 'machine_code','code');
    }
}
