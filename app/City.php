<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'city';


    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];
	 
	public function State()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
	
	public function Distrcits()
    {
        return $this->hasMany(Distrcits::class, 'city_id');
    }
	
	public function Ticketings()
    {
        return $this->hasMany(Ticketing::class, 'city_id');
    }
}
