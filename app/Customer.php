<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   protected $table = 'customer';
	
    protected $guarded = [];
	
	public function contracts()
    {
        return $this->hasMany(Contracts::class, 'customer_id');
    }
	
	public function sla()
    {
        return $this->hasMany(SlaCustomer::class, 'customer_id');
    }
	
	public function User()
    {
        return $this->hasOne(User::class,'customer_id');
    }

}
