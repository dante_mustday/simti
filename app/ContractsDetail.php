<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractsDetail extends Model
{
    protected $table = 'contracts_detail';
	
    protected $guarded = [];
	
	public function machine()
    {
        return $this->hasMany(Machine::class, 'machine_code','code');
    }
	
	public function contract()
    {
        return $this->belongsTo(Contracts::class, 'contract_id');
    }
}
