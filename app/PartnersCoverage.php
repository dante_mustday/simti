<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnersCoverage extends Model
{
   protected $table = 'partners_coverage';
	
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Partners::class, 'category_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

}
