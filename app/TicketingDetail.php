<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketingDetail extends Model
{
   protected $table = 'ticketing_detail';
	
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Ticketing::class, 'ticketing_code', 'ticketing_code');
    }
	
	public function service()
    {
        return $this->belongsTo(Services::class, 'service_code', 'service_code');
    }

    public function technician()
    {
        return $this->belongsTo(Employee::class, 'technician_id');
    }

}
