<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'state';


    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];
	
	public function Country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
	
	public function City()
    {
        return $this->hasMany(City::class, 'state_id');
    }
}
