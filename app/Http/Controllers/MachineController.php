<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Machine;
use App\Mfptypes;
use Auth;


class MachineController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $machines = Mfptypes::orderBy('type_code',"ASC")->paginate(10);

        return view('system-mgmt/machine/index', ['machines' => $machines]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system-mgmt/machine/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
         Mfptypes::create([
            'name' => $request['name'],
            'type_code' => $request['type_code'],
			'created_by' => Auth::user()->employee_id
        ]);

        return redirect()->intended('system-management/machine');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $machine = Mfptypes::find($id);
        // Redirect to machine list if updating machine wasn't existed
        if ($machine == null || count($machine) == 0) {
            return redirect()->intended('/system-management/machine');
        }

        return view('system-mgmt/machine/edit', ['machine' => $machine]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $machine = Mfptypes::findOrFail($id);
        $input = [
            'name' => $request['name'],
            'type_code' => $request['type_code'],
			'updated_by' => Auth::user()->employee_id
        ];
        $this->validate($request, [
        'name' => 'required|max:60'
        ]);
        Mfptypes::where('id', $id)
            ->update($input);
        
        return redirect()->intended('system-management/machine');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mfptypes::destroy($id);
        return redirect()->intended('system-management/machine');
    }

    /**
     * Search machine from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'name' => $request['name'],
            'type_code' => $request['type_code']
            ];

       $machines = $this->doSearchingQuery($constraints);
       return view('system-mgmt/machine/index', ['machines' => $machines, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = Mfptypes::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
        $this->validate($request, [
        'name' => 'required|max:60|unique:machine',
        'type_code' => 'required|max:6|unique:machine'
    ]);
    }
}
