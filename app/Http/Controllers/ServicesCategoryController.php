<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicesCategory;

class ServicesCategoryController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = ServicesCategory::paginate(10);

        return view('system-mgmt/service-category/index', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system-mgmt/service-category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
         ServicesCategory::create([
            
            'name' => $request['name'],
            'price' => $request['price']
        ]);

        return redirect()->intended('system-management/service-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = ServicesCategory::find($id);
        // Redirect to service list if updating service wasn't existed
        if ($service == null || count($service) == 0) {
            return redirect()->intended('/system-management/service-category');
        }

        return view('system-mgmt/service-category/edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = ServicesCategory::findOrFail($id);
        $input = [
            
            'name' => $request['name'],
            'price' => $request['price']
        ];
        $this->validate($request, [
        'name' => 'required|max:60'
        ]);
        ServicesCategory::where('id', $id)
            ->update($input);
        
        return redirect()->intended('system-management/service-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServicesCategory::where('id', $id)->delete();
        return redirect()->intended('system-management/service-category');
    }

    /**
     * Search service from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            
            'name' => $request['name'],
            'price' => $request['price']
            ];

       $services = $this->doSearchingQuery($constraints);
       return view('system-mgmt/service-category/index', ['services' => $services, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = ServicesCategory::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
        $this->validate($request, [
        'name' => 'required|max:60|unique:service_category',
        'price' => 'required|max:6'
    ]);
    }
}
