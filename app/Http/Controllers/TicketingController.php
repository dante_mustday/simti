<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticketing;
use App\Customer;
use App\Contracts;
use App\TicketingDetail;
use App\TicketingRequest;
use App\City;
use App\User;
use App\Sla;
use App\Services;
use App\Partners;
use App\Employee;
use App\ServicesCategory;
use Auth;
use App\ContractsDetail as Details;
use DB;

class TicketingController extends Controller
{
        /**
     * Create a new contdoller instance.
     *
     * @return void
     */
    public function __construct()
    {
      DB::enableQueryLog();
        $this->middleware('auth');
    }

    /**
     * Display a listing of tde resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( Auth::user()->roles == 4 || Auth::user()->roles == 5 || Auth::user()->roles == 0){
            $ticketing = Ticketing::with('technician')->where('status',0)->paginate(10);
        }else{
            $ticketing = Ticketing::where('technician_id',Auth::user()->employee_id)->where('status',0)->paginate(10);
        }
        return view('ticketing-mgmt/index', ['ticketings' => $ticketing]);
    }

     public function emergency()
    {
        if( Auth::user()->roles == 4 || Auth::user()->roles == 5 || Auth::user()->roles == 0){
            $ticketing = Ticketing::where('sla_id',1)->paginate(10);
        }else{
            $ticketing = Ticketing::where('sla_id',1)->where('technician_id',Auth::user()->employee_id)->paginate(10);
        }
        return view('ticketing-mgmt/index', ['ticketings' => $ticketing]);
    }

    public function finish()
    {
        if( Auth::user()->roles == 4 || Auth::user()->roles == 5 || Auth::user()->roles == 0){
            $ticketing = Ticketing::where('status',1)->paginate(10);
        }else{
            $ticketing = Ticketing::where('status',1)->where('technician_id',Auth::user()->employee_id)->paginate(10);
        }
        return view('ticketing-mgmt/index', ['ticketings' => $ticketing]);
    }

    /**
     * Show tde form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
		$categorys = ServicesCategory::all();
        $enginer = Employee::where('position',3)->get();
        $cities = City::all();
        $sla = Sla::all();
        return view('ticketing-mgmt/create',['customers' => $customers,'technicians' => $enginer,'cities' => $cities,'slas'=>$sla,'categorys'=>$categorys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
        $startTime = date($request['date']." ".$request['call_time']);
        $sla = Sla::find($request['sla_id']);
        //add 1 hour to time
        $cenvertedTime = date('Y-m-d H:i:s',strtotime('+'.$sla->maximum.' hour',strtotime($startTime)));
		$time=$sla->maximum;
		$x=explode('.',$time);
		$cekurut = Ticketing::where('open_ticket_date',$request['date'])->get();
    $exist = count($cekurut);
		if($exist>=1){
			if($exist<10){

				$urut = "000".$exist;
			}else if($exist<100){
				$urut = "00".$exist;
      }else if($exist<1000){
				$urut = "0".$exist;
			}else{
				$urut="0001";
			}
		}else{
			$urut="0001";
		}
    $date=date_create($request['date']);

		$detail = Details::find($request['machine']);
        $ticketing = New Ticketing();
        $ticketing->ticketing_code      = "KSK".date_format($date,"yymd").$urut;
		    $ticketing->contract_detail_id  = $request['machine'];
        $ticketing->machine_code        = $detail->machine_code;
		    $ticketing->ask_code       		  = $detail->ask_code;
		    $ticketing->serial_number       = $detail->serial_number;
		    $ticketing->machine		          = $detail->machine;
        $ticketing->is_wo               = $request['type'];
        $ticketing->customer_id         = $request['customer'];
        $ticketing->technician_id       = $request['technician'];
        $ticketing->contract_id         = $request['contract'];
		    $ticketing->category         	  = $request['category'];
        $ticketing->machine_location    = $request['location'];
        $ticketing->city_id             = $request['city_id'];
        $ticketing->sla_id              = $request['sla_id'];
		    $ticketing->maximum_resolved    = $x[0].":00";
        $ticketing->open_ticket_date    = $request['date'];
        $ticketing->limit_ticket_date   = $cenvertedTime;
        $ticketing->call_time           = $request['call_time'];
        $ticketing->call_phone          = $request['call_phone'];
        $ticketing->call_name           = $request['call_name'];
        $ticketing->status              = 0;
        $ticketing->save();

        return redirect()->intended('ticketing');
    }


    public function addAction(Request $request,$id)
    {
        //$this->validateInput($request);

        $ticketing = New TicketingDetail();
        $ticketing->ticketing_code      = $id;
        $ticketing->service_code        = $request['service'];

        $ticketing->visit_date          = $request['date'];
        $ticketing->start               = $request['start'];
        $ticketing->finish              = $request['finish'];
        $ticketing->description         = $request['description'];
        $ticketing->action              = $request['action'];
        $ticketing->result              = $request['result'];
        $ticketing->mono_meter          = $request['mono'];
        $ticketing->color_meter         = $request['color'];
        $ticketing->total_meter         = $request['total'];
        $ticketing->save();
		 if($request['is_wo']==1){
			$detail = Details::find($request['id_detail']);
			$detail->location          = $request['location'];
			$detail->ask_code          = $request['ask_code'];
			$detail->serial_number     = $request['serial_number'];
			$detail->save();
		}
        $return = "<tr>";
        $return .= "<td>".$request['date']."</td>";
        $return .= "<td>".$request['start']."</td>";
        $return .= "<td>".$request['finish']."</td>";
        $return .= "<td>".$request['action']."</td>";
        $return .= "<td>".$request['description']."</td>";
        $return .= "<td>".$request['result']."</td>";
        $return .= "<td><a href='javascript:void(0)' class='btn tbn-danger' onclick='delete_action(".$ticketing->id.")'>Delete</a></td>";
        $return .= "</tr>";
        return $return;
    }

    public function approveRequest($id,$ticket_id)
    {
        $request = TicketingRequest::find($id);
        $request->supervisor_id      = Auth::user()->employee_id;
        $request->is_approved        = 1;
        $request->status              = "Approved";
        $request->save();

        $ticketing = Ticketing::find($ticket_id);
        $ticketing->approval_status      = 2;
        $ticketing->save();
        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

    public function rejectRequest($id,$ticket_id)
    {
        $request = TicketingRequest::find($id);
        $request->supervisor_id      = Auth::user()->employee_id;
        $request->is_approved        = 2;
        $request->status             = "Disapproved";
        $request->save();

        $ticketing = Ticketing::find($ticket_id);
        $ticketing->approval_status      = 3;
        $ticketing->save();
        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

	public function cancelRequest($id,$ticket_id)
    {
        $request = TicketingRequest::find($id);
        $request->supervisor_id      = Auth::user()->employee_id;
        $request->is_approved        = 3;
        $request->status             = "Cancel Not Used";
        $request->save();

        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

	public function approveClosed($ticket_id)
    {
        $ticketing = Ticketing::with('details')->where('id',$ticket_id)->first();
        $ticketing->status      = 1;
		$ticketing->approval_closed_ticket_date = date("Y-m-d h:i:s");

		$sumtime = 0 ;
		foreach($ticketing->details as $row){
			$time = $row->finish - $row->start;
			$sumtime = $sumtime + $time;
		}
		$time=$sumtime;
		$x=explode('.',$time);
		$min=60*($x[1]/10);
		$ticketing->real_resolved = $x[0].":".$min;
		$ticketing->save();
        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

	public function declineClosed($ticket_id)
    {
        $ticketing = Ticketing::find($ticket_id);
        $ticketing->status      = 0;
        $ticketing->save();
        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

    public function closed($ticket_id)
    {
        $ticketing = Ticketing::find($ticket_id);
        $ticketing->status      = 1;
		$ticketing->closed_ticket_date      = date("Y-m-d h:i:s");
        $ticketing->save();
        return redirect()->intended('ticketing/add/'.$ticket_id);
    }

    public function addRequest(Request $request,$id)
    {
        //$this->validateInput($request);

        $ticketing = New TicketingRequest();
        $ticketing->ticketing_code      = $id;
        //$ticketing->service_code        = $request['service'];
        //$ticketing->is_wo               =  $request['type'];
        $ticketing->reason              = $request['reason'];
        $ticketing->status              = "Waiting For Approval";
        $ticketing->save();
        $return = "<tr>";
        $return .= "<td>-</td>";
        $return .= "<td>".$request['reason']."</td>";
        $return .= "<td>Waiting For Approval</td>";
        $return .= "<td>-</td>";
        $return .= "<td><a href='javascript:void(0)' class='btn tbn-danger' onclick='delete_action(".$ticketing->id.")'>Delete</a></td>";
        $return .= "</tr>";

        $ticketing = Ticketing::where('ticketing_code',$id)->first();
        $ticketing->approval_status      = 1;
        $ticketing->save();

        return $return;
    }

	 public function updateRequest(Request $request,$id)
    {
        //$this->validateInput($request);
		$post = $request->all();
        $ticketing = TicketingRequest::find($id);
        $ticketing->sparepart_code      = $post['code'];
		$ticketing->sparepart_name      = $post['name'];
        $ticketing->status              = "Already Installed";
        $ticketing->save();

        return $return;
    }

    /**
     * Display tde specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show tde form for editing tde specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticketing = Ticketing::with('details')->where('id',$id)->first();
        // Redirect to ticketing list if updating ticketing wasn't existed
        if ($ticketing == null || count($ticketing) == 0) {
            return redirect()->intended('ticketing');
        }

        $customers = Customer::all();
		$categorys = ServicesCategory::all();
        $enginer = Employee::where('position',3)->get();
		$contract = Contracts::with('details')->where('customer_id',$ticketing->customer_id)->get();

        $cities = City::all();
        $sla = Sla::all();
        return view('ticketing-mgmt/edit',['ticketing' => $ticketing,
		'customers' => $customers,
		'contracts' => $contract,
		'technicians' => $enginer,
		'cities' => $cities,
		'slas'=>$sla,
		'categorys'=>$categorys]);
    }

    public function add($id)
    {
        $ticketing = Ticketing::with('details','technician','customer','machine','sla','contract','request')->where('id',$id)->first();

        // Redirect to ticketing list if updating ticketing wasn't existed
        if ($ticketing == null || count($ticketing) == 0) {
            return redirect()->intended('ticketing');
        }
        if($ticketing->approval_status==2){
			$services = Services::all();
		}else{
			$services = Services::where('need_approval',0)->get();
		}
        return view('ticketing-mgmt/add', ['ticketing' => $ticketing,'services' => $services]);
    }

    public function storeaction(Request $request, $id)
    {
        $ticketing = ticketing::findOrFail($id);
        $this->validateInput($request);

        $action = New TicketingDetail();
        $action->ticketing_code      = $ticketing->ticketing_code;
        $action->machine_code        = $request['machine'];
        $action->is_wo               =  $request['type'];
        $action->customer_id         = $request['customer'];
        $action->technician_id       = $request['technician'];
        $action->contract_id         = $request['contract'];
        $action->machine_location    = $request['location'];
        $action->open_ticket_date    = $request['date'];
        $action->call_time           = $request['call_time'];
        $action->call_phone          = $request['call_phone'];
        $action->call_name           = $request['call_name'];
        $action->save();

        return redirect()->intended('ticketing');
    }

    /**
     * Update tde specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function updateTechnician(Request $request, $id)
    {
        $ticketing = Ticketing::find($id);
		$startTime = date($request['date']." ".$request['call_time']);
		$sla = Sla::find($ticketing->sla_id);
		$cenvertedTime = date('Y-m-d H:i:s',strtotime('+'.$sla->maximum.' hour',strtotime($startTime)));
        $ticketing->technician_id       = $request['technician'];
        $ticketing->machine_location    = $request['location'];
        $ticketing->open_ticket_date    = $request['date'];
        $ticketing->limit_ticket_date   = $cenvertedTime;
        $ticketing->call_time           = $request['call_time'];
        $ticketing->call_phone          = $request['call_phone'];
        $ticketing->call_name           = $request['call_name'];
        $ticketing->save();

        return redirect()->intended('ticketing');
    }
    public function update(Request $request, $id)
    {
        $ticketing = Ticketing::findOrFail($id);
        $input = [
            'name' => $request['name'],
            'code' => $request['code']
        ];
        $this->validate($request, [
        'name' => 'required|max:60'
        ]);
        Ticketing::where('id', $id)
            ->update($input);

        return redirect()->intended('ticketing');
    }

    /**
     * Remove tde specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ticketing::where('id', $id)->softDeletes();
        return redirect()->intended('ticketing');
    }

    /**
     * Search ticketing from database base on some specific constdaints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constdaints = [
            'ticketing_number' => $request['number']
            ];

       $ticketing = $this->doSearchingQuery($constdaints);
       return view('ticketing-mgmt/index', ['ticketing' => $ticketing, 'searchingVals' => $constdaints]);
    }

    private function doSearchingQuery($constdaints) {
        $query = Ticketing::query();
        $fields = array_keys($constdaints);
        $index = 0;
        foreach ($constdaints as $constdaint) {
            if ($constdaint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constdaint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
       /* $this->validate($request, [
            'number' => 'required|max:60|unique:ticketing_number'
        ]);*/
    }
}
