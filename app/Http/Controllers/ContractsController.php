<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts;
use App\Mfptypes;
use App\Customer;
use App\Employee;
use App\ContractsDetail as Details;
use App\ContractsDetailCopy as DetailsCopy; 

class ContractsController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contracts = Contracts::with('customer','details')->paginate(10);

        return view('contracts-mgmt/index', ['contracts' => $contracts]);
    }
	
	public function updatedata()
    {
        $contracts = DetailsCopy::where('contract_number','!=','-')->orderBy('contract_number', 'asc')->orderBy('machine_code', 'asc')->get();
		
		foreach($contracts as $row){
		
			$contract = Details::where('contract_number','=',$row->contract_number)->where('machine_code','=',$row->machine_code)->where('ask_code','=',null)
			  ->update(['ask_code' => $row->ask_code,'location'=>$row->location,'serial_number'=>$row->serial_number]);
			/*$contract = Details::where('contract_number','=',$row->contract_number)->where('machine_code','=',$row->machine_code)->first();
			$contract->ask_code     = $row->ask_code;
			$contract->location         = $row->location;
			$contract->serial_number  = $row->serial_number;
			$contract->save();*/
			
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Mfptypes::all();
        $customers = Customer::all();
        $sales = Employee::whereIn('position',array(1,2))->get();
        return view('contracts-mgmt/create',['types' => $types,'customers' => $customers,'sales' => $sales]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
        $contract = New Contracts();
        $contract->customer_id      = $request['customer'];
        $contract->sales_id         = $request['sales'];
        $contract->contract_number  = $request['number'];
        $contract->contract_date    = $request['date'];
        $contract->customer_po_number = $request['po_number'];
        $contract->customer_po_date = $request['po_date'];
        $contract->save();
        $cmesin = count($request['machine_code']);
        $data = array();

        for($i=0;$i<$cmesin;$i++){
            $z=$request['quantity'][$i];
            for($y=0;$y<$z;$y++){
                $data[]=array(
                        'contract_id'=>$contract->id,
                        'contract_number'=>$request['number'],
                        'machine_code'=>$request['machine_code'][$i],
                        'machine'=>$request['machine_name'][$i]
                    );

            }
        }

        Details::insert($data);
        return redirect()->intended('contract');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contracts::with('details')->where('id',$id)->first();
        // Redirect to contract list if updating contract wasn't existed
        if ($contract == null || count($contract) == 0) {
            return redirect()->intended('contract');
        }
        $types = Mfptypes::all();
        $customers = Customer::all();
        $sales = Employee::all();
      
        return view('contracts-mgmt/edit', ['contract' => $contract,'types' => $types,'customers' => $customers,'sales' => $sales]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $contract = Contracts::findOrFail($id);
        $contract->customer_id      = $request['customer'];
        $contract->sales_id         = $request['sales'];
        $contract->contract_number  = $request['number'];
        $contract->contract_date    = $request['date'];
        $contract->customer_po_number = $request['po_number'];
        $contract->customer_po_date = $request['po_date'];
        $contract->save();
        $cmesin = count($request['machine_code']);
        $data = array();

        for($i=0;$i<$cmesin;$i++){
            $z=$request['quantity'][$i];
            for($y=0;$y<$z;$y++){
                $data[]=array(
                        'contract_id'=>$id,
                        'contract_number'=>$request['number'],
                        'machine_code'=>$request['machine_code'][$i],
                        'machine'=>$request['machine_name'][$i]
                    );

            }
        }
        Details::insert($data);
        return redirect()->intended('contract/'.$id.'/edit');
    }
	
	public function updatemachine(Request $request)
    {
		$id = $request['id_detail'];
        $contract = Details::findOrFail($id);
        $contract->ask_code      = $request['ask_code'];
        $contract->color_meter         = $request['color'];
        $contract->mono_meter  = $request['mono'];
        $contract->location    = $request['location'];
		$contract->serial_number    = $request['serial_number'];
        $contract->save();
		echo "Success";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contracts::where('id', $id)->delete();
        return redirect()->intended('contract');
    }
	
	public function destroydetail($contract,$id)
    {
        Details::where('id', $id)->delete();
        return redirect()->intended('contract/'.$contract.'/edit');
    }

    /**
     * Search contract from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'contract_number' => $request['contract_number']
            ];

       $contracts = $this->doSearchingQuery($constraints);
       return view('contracts-mgmt/index', ['contracts' => $contracts, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = Contracts::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
       /* $this->validate($request, [
            'number' => 'required|max:60|unique:contract_number'
        ]);*/
    }
}
