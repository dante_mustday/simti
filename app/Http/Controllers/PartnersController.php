<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Partners;
use App\PartnersCoverage;
use App\Services;
use App\City;
use App\User;


class PartnersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = DB::table('partners')
        ->leftJoin('city', 'partners.city_id', '=', 'city.id')
        ->select('partners.firstname as partner_name', 'partners.*','city.name as city_name')->paginate(5);

        return view('partners/index', ['partners' => $partners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('partners/create', ['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
        // Upload image
        $partner = New Partners();
        $partner->lastname         = $request['lastname'];
        $partner->firstname        = $request['firstname'];
        $partner->address          = $request['address'];
        $partner->city_id      	   = $request['city_id'];
        $partner->email            = $request['email'];
        $partner->phone            = $request['phone'];
        $partner->save();
        if($partner){
        	$username = explode("@",$request['email']);
            User::create([
                'username'      => $username[0],
                'email'         => $request['email'],
                'password'      => bcrypt("partners"),
                'firstname'     => $request['firstname'],
                'lastname'      => $request['lastname'],
                'type'			=> 1,
                'roles'			=> 10,
                'employee_id'	=> 0,
                'partner_id'    => $partner->id
            ]);    
        }

        return redirect()->intended('/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $partner = Partners::find($id);
        // Redirect to state list if updating state wasn't existed
        if ($partner == null || count($partner) == 0) {
            return redirect()->intended('/partners');
        }
        $cities = City::all();
        return view('partners/view', ['partner' => $partner, 'cities' => $cities]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partners::find($id);
        // Redirect to state list if updating state wasn't existed
        if ($partner == null || count($partner) == 0) {
            return redirect()->intended('/partners');
        }
        $cities = City::all();
        return view('partners/edit', ['partner' => $partner, 'cities' => $cities, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validateInput($request);
        $partner = Partners::findOrFail($id);
        $partner->lastname         = $request['lastname'];
        $partner->firstname        = $request['firstname'];
        $partner->address          = $request['address'];
        $partner->city_id      	   = $request['city_id'];
        $partner->email            = $request['email'];
        $partner->phone            = $request['phone'];
        $partner->save();
        if($partner){
        	$user = User::where('partner_id',$id)->first();
			$user->email         = $request['email'];
	        $user->lastname         = $request['lastname'];
	        $user->firstname        = $request['firstname'];
	        $user->save();
   
        }

        return redirect()->intended('/partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Partners::where('id', $id)->delete();
         return redirect()->intended('/partners');
    }

    /**
     * Search state from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'firstname' => $request['firstname'],
            'city.name' => $request['city_name']
            ];
        $partners = $this->doSearchingQuery($constraints);
        $constraints['department_name'] = $request['department_name'];
        return view('partners/index', ['partners' => $partners, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = DB::table('partners')
        ->leftJoin('city', 'partners.city_id', '=', 'city.id')
        ->select('partners.firstname as partner_name', 'partners.*','city.name as city_name');
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where($fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }

     /**
     * Load image resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */


    private function validateInput($request) {
        $this->validate($request, [
            'lastname' => 'required|max:60',
            'firstname' => 'required|max:60',
            'phone' => 'required|max:60',
            'address' => 'required|max:120',
            'email' => 'required'
        ]);
    }

    public function addCoverage(Request $request,$id)
    {
        //$this->validateInput($request);

        $coverage = New PartnersCoverage();
        $coverage->partner_id	      	= $id;
        $coverage->city_id        		= $request['city'];
        $coverage->price_install        = $request['install'];
        $coverage->price_service        = $request['service'];
        $coverage->price_maintenance    = $request['maintenance'];
        $coverage->price_toner       	= $request['toner'];
        $coverage->save();

        $return = "<tr>";
        $return .= "<td>".$request['cityname']."</td>";
        $return .= "<td>".$request['install']."</td>";
        $return .= "<td>".$request['service']."</td>";
        $return .= "<td>".$request['maintenance']."</td>";
        $return .= "<td>".$request['toner']."</td>";
        $return .= "<td><a href='javascript:void(0)' class='btn tbn-danger' onclick='delete_action(".$coverage->id.")'>Delete</a></td>";
        $return .= "</tr>";
        return $return;
    }

}
