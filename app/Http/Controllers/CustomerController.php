<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\City;
use App\User;
use App\Sla;
use App\SlaCustomer as Details;

class CustomerController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::with('User')->paginate(10);

        return view('customer-mgmt/index', ['customers' => $customer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $cities = City::all();
		 $sla = Sla::all();
        return view('customer-mgmt/create', ['cities' => $cities,'sla'=>$sla]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
        $new = New Customer();
		$new->city_id = $request['city_id'];
        $new->name = $request['name'];
		$new->code = $request['code'];
        $new->address = $request['address'];
        $new->email = $request['email'];
        $new->phone = $request['phone'];
        $new->pic_name = $request['pic_name'];
        $new->pic_phone = $request['pic_phone'];
        $new->pic_email = $request['pic_email'];
        $new->save();

		$priority = count($request['priority']);
        $data = array();

        for($i=0;$i<$priority;$i++){
            $data[]=array(
                'customer_id'=>$new->id,
                'code'=>$request['code']."-00".$i,
                'name'=>$request['priority'][$i],
                'maximum'=>$request['maximum'][$i]
            );
        }
        Details::insert($data);

        return redirect()->intended('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::with('sla')->where('id',$id)->first();
		 $sla = Sla::all();
        // Redirect to customer list if updating customer wasn't existed
        if ($customer == null || count($customer) == 0) {
            return redirect()->intended('/customer');
        }
         $cities = City::all();
        return view('customer-mgmt/edit', ['customer' => $customer,'cities' => $cities,'sla'=>$sla]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $input = [
            'city_id' => $request['city_id'],
            'name' => $request['name'],
			'code' => $request['code'],
            'address' => $request['address'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'pic_name' => $request['pic_name'],
            'pic_phone' => $request['pic_phone'],
            'pic_email' => $request['pic_email']
        ];
        $this->validate($request, [
        'name' => 'required|max:60'
        ]);
        Customer::where('id', $id)
            ->update($input);
        $maximum = count($request['maximum']);
        $data = array();
		      $start = count($customer->sla);

          if($maximum>0){
        for($i=$start;$i<($maximum+$start);$i++){
            $data[]=array(
                'customer_id'=>$id,
                'code'=>$request['code']."-00".$i,
                'name'=>$request['priority'][$i],
                'maximum'=>$request['maximum'][$i]
            );
        }
        Details::insert($data);
      }
        return redirect()->intended('customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::where('id', $id)->softDeletes();
        return redirect()->intended('customer');
    }

    /**
     * Search customer from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'name' => $request['name'],
            'email' => $request['email']
            ];

       $customer = $this->doSearchingQuery($constraints);
       return view('customer-mgmt/index', ['customers' => $customer, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = Customer::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
        $this->validate($request, [
        'name' => 'required|max:60|unique:customer',
        'email' => 'required|max:60|unique:customer'
    ]);
    }
}
