<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sla;

class SlaController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slas = Sla::paginate(10);

        return view('system-mgmt/sla/index', ['slas' => $slas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system-mgmt/sla/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
         Sla::create([
            'name' => $request['name'],
            'code' => $request['code'],
            'maximum' => $request['maximum']
        ]);

        return redirect()->intended('system-management/sla');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sla = sla::find($id);
        // Redirect to sla list if updating sla wasn't existed
        if ($sla == null || count($sla) == 0) {
            return redirect()->intended('/system-management/sla');
        }

        return view('system-mgmt/sla/edit', ['sla' => $sla]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sla = sla::findOrFail($id);
        $input = [
            'name' => $request['name'],
            'code' => $request['code'],
            'maximum' => $request['maximum']
        ];
        $this->validate($request, [
        'name' => 'required|max:60'
        ]);
        sla::where('id', $id)
            ->update($input);
        
        return redirect()->intended('system-management/sla');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        sla::where('id', $id)->softDeletes();
        return redirect()->intended('system-management/sla');
    }

    /**
     * Search sla from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'name' => $request['name'],
            'code' => $request['code']
            ];

       $slas = $this->doSearchingQuery($constraints);
       return view('system-mgmt/sla/index', ['slas' => $slas, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = sla::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }

    private function validateInput($request) {
        $this->validate($request, [
        'name' => 'required|max:60|unique:sla',
        'code' => 'required|max:6|unique:sla',
        'maximum' => 'required|unique:sla'
        ]);
    }
}
