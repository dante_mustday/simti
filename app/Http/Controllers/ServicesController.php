<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;

class ServicesController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::with('parent')->paginate(10);
       
        return view('system-mgmt/service/index', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system-mgmt/service/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
         Services::create([
            'category_id' => $request['category_id'],
			'need_approval' => $request['need_approval'],
            'service_name' => $request['service_name'],
            'service_code' => $request['service_code']
        ]);

        return redirect()->intended('system-management/service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Services::find($id);
        // Redirect to service list if updating service wasn't existed
        if ($service == null || count($service) == 0) {
            return redirect()->intended('/system-management/service');
        }

        return view('system-mgmt/service/edit', ['service' => $service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Services::findOrFail($id);
        $input = [
            'category_id' => $request['category_id'],
            'service_name' => $request['service_name'],
			'need_approval' => $request['need_approval'],
            'service_code' => $request['service_code']
        ];
        $this->validate($request, [
        'service_name' => 'required|max:60'
        ]);
        Services::where('id', $id)
            ->update($input);
        
        return redirect()->intended('system-management/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Services::where('id', $id)->delete();
        return redirect()->intended('system-management/service');
    }

    /**
     * Search service from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'category_id' => $request['category_id'],
            'service_name' => $request['service_name'],
            'service_code' => $request['service_code']
            ];

       $services = $this->doSearchingQuery($constraints);
       return view('system-mgmt/service/index', ['services' => $services, 'searchingVals' => $constraints]);
    }

    private function doSearchingQuery($constraints) {
        $query = Services::query();
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }
    private function validateInput($request) {
        $this->validate($request, [
        //'service_name' => 'required|max:60|unique:services',
        'service_code' => 'required|max:6|unique:services'
    ]);
    }
}
