<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticketing;
use App\Customer;
use App\Contracts;
use App\TicketingDetail;
use App\TicketingRequest;
use App\City;
use App\User;
use App\Sla;
use App\Services;
use App\Partners;
use App\Employee;
use Auth;
use App\ContractsDetail as Details;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    $year = date("Y");
    $da = date("Y-m-d");
    $q1=date_create($year."-07-01");
    if($da < $q1){
      $start = date_create($year."-01-01");
      $end = date_create($year."-06-30");
    }else{
      $start = date_create($year."-07-01");
      $end = date_create($year."-12-31");
    }

		$date = strtotime($da .' -1 months');
		$final=date('Y-m-d', $date);
		$lmonth = substr($final,5,2);
		$lyear = substr($final,8,2);
		$achivementThisMonth = Ticketing::where('deleted_at','=',null)->where('closed_ticket_date','<=','limit_ticket_date')->whereMonth('open_ticket_date', '=', date('m'))->whereMonth('open_ticket_date', '=', date('Y'))->count();
		$achivementLastMonth = Ticketing::where('deleted_at','=',null)->where('closed_ticket_date','<=','limit_ticket_date')->whereMonth('open_ticket_date', '=', $lmonth)->whereMonth('open_ticket_date', '=', $lyear)->count();
		$achivement = Ticketing::where('deleted_at','=',null)->where('closed_ticket_date','<=','limit_ticket_date')->count();
		$ticketing  = Ticketing::where('deleted_at','=',null)->count();
		$tcontract 	= Contracts::where('deleted_at','=',null)->count();
		$tmachine 	= Details::where('deleted_at','=',null)->count();
		$tcust 		= Customer::where('deleted_at','=',null)->count();
		$partner 	= Partners::where('deleted_at','=',null)->count();
    $emergencyFull  = Ticketing::whereBetween('created_at', [$start, $end])
      ->where('sla_id','=',1)
      ->where('deleted_at','=',null)
      ->count();
    $lowFull  = Ticketing::whereBetween('created_at', [$start, $end])
      ->where('sla_id','=',2)
      ->where('deleted_at','=',null)
      ->count();
    $mediumFull  = Ticketing::whereBetween('created_at', [$start, $end])
      ->where('sla_id','=',3)
      ->where('deleted_at','=',null)
      ->count();
    $highFull  = Ticketing::whereBetween('created_at', [$start, $end])
      ->where('sla_id','=',4)
      ->where('deleted_at','=',null)
      ->count();
    $emergencyAchive  = Ticketing::whereBetween('created_at', [$start, $end])
        ->where('closed_ticket_date','<=','limit_ticket_date')
        ->where('sla_id','=',1)
        ->where('deleted_at','=',null)
        ->count();
    $lowAchive  = Ticketing::whereBetween('created_at', [$start, $end])
        ->where('closed_ticket_date','<=','limit_ticket_date')
        ->where('sla_id','=',2)
        ->where('deleted_at','=',null)
        ->count();
    $mediumAchive  = Ticketing::whereBetween('created_at', [$start, $end])
        ->where('closed_ticket_date','<=','limit_ticket_date')
        ->where('sla_id','=',3)
        ->where('deleted_at','=',null)
        ->count();
    $highAchive  = Ticketing::whereBetween('created_at', [$start, $end])
        ->where('closed_ticket_date','<=','limit_ticket_date')
        ->where('sla_id','=',4)
        ->where('deleted_at','=',null)
        ->count();
    $full = array("emergency"=>$emergencyFull,"low"=>$lowFull,"medium"=>$mediumFull,"high"=>$highFull);
    $achive = array("emergency"=>$emergencyAchive,"low"=>$lowAchive,"medium"=>$mediumAchive,"high"=>$highAchive);
		$compare 	= (($achivementThisMonth+1)-($achivementLastMonth+1))/(1+$achivementLastMonth) * 100;
        return view('dashboard',[
		'contract' => $tcontract,
		'machine'=>$tmachine,
		'customer'=>$tcust,
		'partner'=>$partner,
		'achivement'=>$achivement,
		'ticket'=>$ticketing,
		'achive_month'=>$achivementThisMonth,
    'start'=>$start,
    'end'=>$end,
    'full'=>$full,
    'achive'=>$achive,
		'achive_compare' => $compare
		]);
    }
}
