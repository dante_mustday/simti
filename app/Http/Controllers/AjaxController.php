<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts;
use App\ContractsDetail as Details;
use App\Employee;
use App\Partners;
class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContractByCustomer($param)
    {
        $result = Contracts::where('customer_id',$param)->get();
        return view('ajax/contract',['result'=>$result]);
    }

    public function getMachineByContract($param)
    {
        $result = Details::where('contract_id',$param)->get();
        return view('ajax/machine',['result'=>$result]);
    }
	
	public function getTechnicianByTicketType($param,$city){
		if($param==0){
			$result = Employee::where('position',3)->where('deleted_at','=',null)->get();
		}else{
			$result = Partners::where('city_id','=',$city)->where('deleted_at','=',null)->get();
		}
		return view('ajax/technician',['result'=>$result]);
	}
}
