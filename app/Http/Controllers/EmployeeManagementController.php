<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Employee;
use App\City;
use App\State;
use App\Country;
use App\Department;
use App\Division;
use App\User;

class EmployeeManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = DB::table('employees')
        ->leftJoin('city', 'employees.city_id', '=', 'city.id')
        ->leftJoin('department', 'employees.department_id', '=', 'department.id')
        ->leftJoin('state', 'employees.state_id', '=', 'state.id')
        ->leftJoin('country', 'employees.country_id', '=', 'country.id')
        ->leftJoin('division', 'employees.division_id', '=', 'division.id')
		->leftJoin('users', 'employees.id', '=', 'users.employee_id')
        ->select('employees.*','users.email', 'department.name as department_name', 'department.id as department_id', 'division.name as division_name', 'division.id as division_id')
		->where('employees.deleted_at',null)
        ->paginate(10);
		
		$position=array(
			"1"=>"Sales",
			"2"=>"Supervisor Sales",
			"3"=>"Enginer",
			"4"=>"Super Admin",
			"5"=>"Supervisor Enginer",
            "6"=>"Admin Support"
		 );

        return view('employees-mgmt/index', ['employees' => $employees,'position'=>$position]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        $states = State::all();
        $countries = Country::all();
        $departments = Department::all();
        $divisions = Division::all();
        return view('employees-mgmt/create', ['cities' => $cities, 'states' => $states, 'countries' => $countries,
        'departments' => $departments, 'divisions' => $divisions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);
		$this->validate($request, [
				'email' => 'required|email|unique:users,email',
			]);
        // Upload image
        //$path = $request->file('picture')->store('avatars');
        $employee = New Employee();
        $employee->lastname         = $request['lastname'];
        $employee->firstname        = $request['firstname'];
        $employee->middlename       = $request['middlename'];
        $employee->address          = $request['address'];
        $employee->division_id      = $request['division_id'];
        //$employee->roles            = $request['position'];
        $employee->company_id       = 0;
        $employee->position         = $request['position'];
        $employee->save();
        if($employee){
            User::create([
                'username'      => $request['username'],
                'email'         => $request['email'],
                'password'      => bcrypt("admin"),
                'firstname'     => $request['firstname'],
                'lastname'      => $request['lastname'],
                'roles'         => $request['position'],
                'employee_id'   => $employee->id
            ]);    
        }

        return redirect()->intended('/employee-management');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::with('User')->where('id',$id)->first();
        // Redirect to state list if updating state wasn't existed
        if ($employee == null || count($employee) == 0) {
            return redirect()->intended('/employee-management');
        }
        $cities = City::all();
        $states = State::all();
        $countries = Country::all();
        $departments = Department::all();
        $divisions = Division::all();
        return view('employees-mgmt/edit', ['employee' => $employee, 'cities' => $cities, 'states' => $states, 'countries' => $countries,
        'departments' => $departments, 'divisions' => $divisions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateInput($request);
        // Upload image
		$user = User::where('employee_id',$id)->first();
		$this->validate($request, [
				'email' => 'required|email|unique:users,email,'.$user->id,
			]);
        $employee = Employee::findOrFail($id);
        $employee->lastname         = $request['lastname'];
        $employee->firstname        = $request['firstname'];
        $employee->middlename       = $request['middlename'];
        $employee->address          = $request['address'];
        $employee->division_id      = $request['division_id'];
        $employee->company_id       = 0;
        $employee->position         = $request['position'];
        $employee->save();
        if($employee){
            
			
            $user->lastname         = $request['lastname'];
            $user->firstname        = $request['firstname'];
            $user->roles            = $request['position'];
            $user->email            = $request['email'];
            $user->save();
          
        }

        return redirect()->intended('/employee-management');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {	
         Employee::destroy($id);
		 User::where('employee_id',$id)->delete();
         return redirect()->intended('/employee-management');
    }

    /**
     * Search state from database base on some specific constraints
     *
     * @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $constraints = [
            'firstname' => $request['firstname'],
            'department.name' => $request['department_name']
            ];
        $employees = $this->doSearchingQuery($constraints);
        $constraints['department_name'] = $request['department_name'];
        $position=array(
            "1"=>"Sales",
            "2"=>"Supervisor Sales",
            "3"=>"Enginer",
            "4"=>"Super Admin",
            "5"=>"Supervisor Enginer",
            "6"=>"Admin Support"
         );
        return view('employees-mgmt/index', ['employees' => $employees, 'searchingVals' => $constraints,'position'=>$position]);
    }

    private function doSearchingQuery($constraints) {
        $query = DB::table('employees')
        ->leftJoin('city', 'employees.city_id', '=', 'city.id')
        ->leftJoin('department', 'employees.department_id', '=', 'department.id')
        ->leftJoin('state', 'employees.state_id', '=', 'state.id')
        ->leftJoin('country', 'employees.country_id', '=', 'country.id')
        ->leftJoin('division', 'employees.division_id', '=', 'division.id')
        ->select('employees.firstname as employee_name', 'employees.*','department.name as department_name', 'department.id as department_id', 'division.name as division_name', 'division.id as division_id');
        $fields = array_keys($constraints);
        $index = 0;
        foreach ($constraints as $constraint) {
            if ($constraint != null) {
                $query = $query->where($fields[$index], 'like', '%'.$constraint.'%');
            }

            $index++;
        }
        return $query->paginate(5);
    }

     /**
     * Load image resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function load($name) {
         $path = storage_path().'/app/avatars/'.$name;
        if (file_exists($path)) {
            return Response::download($path);
        }
    }

    private function validateInput($request) {
        $this->validate($request, [
            'lastname' => 'required|max:60',
            'firstname' => 'required|max:60',
            'middlename' => 'required|max:60',
            'address' => 'required|max:120',
            /*'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'zip' => 'required|max:10',
            'age' => 'required',
            'birthdate' => 'required',
            'date_hired' => 'required',
            'department_id' => 'required',*/
            'division_id' => 'required',
            'position' => 'required'
        ]);
    }
	
	private function validateInputUpdate($request) {
        $this->validate($request, [
            'lastname' => 'required|max:60',
            'firstname' => 'required|max:60',
            'middlename' => 'required|max:60',
            'address' => 'required|max:120',
            /*'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'zip' => 'required|max:10',
            'age' => 'required',
            'birthdate' => 'required',
            'date_hired' => 'required',
            'department_id' => 'required',*/
            'division_id' => 'required',
            'position' => 'required'
        ]);
    }

    private function createQueryInput($keys, $request) {
        $queryInput = [];
        for($i = 0; $i < sizeof($keys); $i++) {
            $key = $keys[$i];
            $queryInput[$key] = $request[$key];
        }

        return $queryInput;
    }
}
