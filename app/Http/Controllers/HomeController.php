<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticketing;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	 
	public function live()
    {
        return view('realtime');
    }
	
	public function progress()
    {
        $ticketing = Ticketing::with('customer','technician')->where('status',0)->get();
		$list = "";
		foreach($ticketing as $row){
			$list .="<li>";
			$list .="<div class='panel panel-primary'>";
			$list .="  <div class='panel-heading'>".$row->ticketing_code."</div>";
			$list .="  <div class='panel-body'>";
			$list .="  <p>Customer : ".$row->customer->name."</p>";
			$list .="  <p>Teknisi : ".$row->technician->firstname." ".$row->technician->lastname."</p>";
			$list .="  <p>Mesin : ".$row->machine."</p>";
			$list .="  <p>Lokasi : ".$row->machine_location."</p>";
			$list .="  </div>";
			$list .="</div>";
			$list .="</li>";
		}
		echo $list;
    }
	
	public function emergency()
    {
        $ticketing = Ticketing::with('customer','technician')->where('status',0)->where('sla_id',1)->get();
		$list = "";
		foreach($ticketing as $row){
			$list .="<li>";
			$list .="<div class='panel panel-danger'>";
			$list .="  <div class='panel-heading'>".$row->ticketing_code."</div>";
			$list .="  <div class='panel-body'>";
			$list .="  <p>Customer : ".$row->customer->name."</p>";
			$list .="  <p>Teknisi : ".$row->technician->firstname." ".$row->technician->lastname."</p>";
			$list .="  <p>Mesin : ".$row->machine."</p>";
			$list .="  <p>Lokasi : ".$row->machine_location."</p>";
			$list .="  </div>";
			$list .="</div>";
			$list .="</li>";
		}
		echo $list;
    }
	
	public function deadline()
    {
        $ticketing = Ticketing::with('customer','technician')->where('status',0)->where('closed_ticket_date','=',null)->get();
		$list = "";
		foreach($ticketing as $row){
			$list .="<li>";
			$list .="<div class='panel panel-warning'>";
			$list .="  <div class='panel-heading'>".$row->ticketing_code."</div>";
			$list .="  <div class='panel-body'>";
			$list .="  <p>Customer : ".$row->customer->name."</p>";
			$list .="  <p>Teknisi : ".$row->technician->firstname." ".$row->technician->lastname."</p>";
			$list .="  <p>Mesin : ".$row->machine."</p>";
			$list .="  <p>Lokasi : ".$row->machine_location."</p>";
			$list .="  </div>";
			$list .="</div>";
			$list .="</li>";
		}
		echo $list;
    }
	
	public function finish()
    {
        $ticketing = Ticketing::with('customer','technician')->where('status',1)->where('closed_ticket_date','!=',null)->get();
		$list = "";
		foreach($ticketing as $row){
			$list .="<li>";
			$list .="<div class='panel panel-success'>";
			$list .="  <div class='panel-heading'>".$row->ticketing_code."</div>";
			$list .="  <div class='panel-body'>";
			$list .="  <p>Customer : ".$row->customer->name."</p>";
			$list .="  <p>Teknisi : ".$row->technician->firstname." ".$row->technician->lastname."</p>";
			$list .="  <p>Mesin : ".$row->machine."</p>";
			$list .="  <p>Lokasi : ".$row->machine_location."</p>";
			$list .="  </div>";
			$list .="</div>";
			$list .="</li>";
		}
		echo $list;
    }
}
