<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
   protected $table = 'services';
	
    protected $guarded = [];

    public function ticketing()
    {
        return $this->hasMany(TicketingDetail::class, 'service_code', 'service_code');
    }

    public function parent()
    {
        return $this->belongsTo(ServicesCategory::class, 'category_id');
    }

}
