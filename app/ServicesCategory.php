<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesCategory extends Model
{
   protected $table = 'service_category';
	
    protected $guarded = [];

    public function child()
    {
        return $this->belongsTo(Services::class, 'category_id');
    }

}
