<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    protected $table = 'partners';
	
    protected $guarded = [];

    public function coverage()
    {
        return $this->hasMany(PartnersCoverage::class, 'partner_id');
    }
}
