<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contracts extends Model
{
    protected $table = 'contracts';
	
    protected $guarded = [];
	
	public function details()
    {
        return $this->hasMany(ContractsDetail::class, 'contract_id');
    }
	
	public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id','id');
    }
}
