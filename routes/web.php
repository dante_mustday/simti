<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware('auth');

Auth::routes();

Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index');
// Route::get('/system-management/{option}', 'SystemMgmtController@index');
Route::resource('profile', 'ProfileController'); 
Route::get('/profile', 'ProfileController@index');
Route::post('/profile/update', 'ProfileController@update')->name('profile.update');

Route::post('user-management/search', 'UserManagementController@search')->name('user-management.search');
Route::resource('user-management', 'UserManagementController');

Route::resource('employee-management', 'EmployeeManagementController');
Route::post('employee-management/search', 'EmployeeManagementController@search')->name('employee-management.search');

Route::resource('system-management/department', 'DepartmentController');
Route::post('system-management/department/search', 'DepartmentController@search')->name('department.search');

Route::resource('system-management/division', 'DivisionController');
Route::post('system-management/division/search', 'DivisionController@search')->name('division.search');

Route::resource('system-management/country', 'CountryController');
Route::post('system-management/country/search', 'CountryController@search')->name('country.search');
 
Route::resource('system-management/state', 'StateController');
Route::post('system-management/state/search', 'StateController@search')->name('state.search');

Route::resource('system-management/city', 'CityController');
Route::post('system-management/city/search', 'CityController@search')->name('city.search');

Route::resource('system-management/machine', 'MachineController');
Route::post('system-management/machine/search', 'MachineController@search')->name('machine.search');

Route::resource('system-management/service', 'ServicesController');
Route::post('system-management/service/search', 'ServicesController@search')->name('service.search');

Route::resource('system-management/service-category', 'ServicesCategoryController');
Route::post('system-management/service-category/search', 'ServicesCategoryController@search')->name('service-category.search');

Route::resource('system-management/sla', 'SlaController');
Route::post('system-management/sla/search', 'SlaController@search')->name('sla.search');

Route::resource('partners', 'PartnersController');
Route::post('partners/search', 'PartnersController@search')->name('partners.search');
Route::post('partners/add-coverage/{id}', 'PartnersController@addCoverage')->name('partners.addcoverage');
 
Route::resource('contract', 'ContractsController'); 
Route::get('contract-update', 'ContractsController@updatedata')->name('contract.updatedata');
Route::get('contract/{contract}/delete-machine/{id}', 'ContractsController@destroydetail')->name('contract.deletemachine');
Route::post('contract/update-machine', 'ContractsController@updatemachine')->name('contract.updatemachine');
Route::post('contract/search', 'ContractsController@search')->name('contract.search');

Route::resource('customer', 'CustomerController'); 
Route::post('customer/search', 'CustomerController@search')->name('customer.search');

Route::resource('ticketing', 'TicketingController'); 
Route::post('ticketing/change-technician/{id}', 'TicketingController@updateTechnician')->name('ticketing.update-technician');
Route::get('ticketing-emergency', 'TicketingController@emergency')->name('ticketing.emergencys');
Route::get('ticketing-finish', 'TicketingController@finish')->name('ticketing.finishs');

Route::get('ticketing/approve-request/{id}/{ticket_id}', 'TicketingController@approveRequest')->name('ticketing.approverequest');
Route::get('ticketing/reject-request/{id}/{ticket_id}', 'TicketingController@rejectRequest')->name('ticketing.rejectrequest');
Route::get('ticketing/cancel-request/{id}/{ticket_id}', 'TicketingController@cancelRequest')->name('ticketing.cancelrequest');
Route::post('ticketing/add-request/{id}', 'TicketingController@addRequest')->name('ticketing.addrequest');
Route::post('ticketing/update-request/{id}', 'TicketingController@updateRequest')->name('ticketing.updaterequest');

Route::get('ticketing/closed/{ticket_id}', 'TicketingController@closed')->name('ticketing.closed');
Route::get('ticketing/decline-closed/{ticket_id}', 'TicketingController@declineClosed')->name('ticketing.declineclosed');
Route::get('ticketing/approve-closed/{ticket_id}', 'TicketingController@approveClosed')->name('ticketing.approveclosed');
Route::post('ticketing/search', 'TicketingController@search')->name('ticketing.search');
Route::get('ticketing/add/{id}', 'TicketingController@add')->name('ticketing.add');
Route::post('ticketing/add-action/{id}', 'TicketingController@addAction')->name('ticketing.addaction');


Route::get('system-management/report', 'ReportController@index');
Route::get('ajax/get_contract_by_customer/{param}', 'AjaxController@getContractByCustomer');
Route::get('ajax/get_machine_by_contract/{param}', 'AjaxController@getMachineByContract');
Route::get('ajax/get_technician_by_ticket_type/{param}/{city}', 'AjaxController@getTechnicianByTicketType');
Route::post('system-management/report/search', 'ReportController@search')->name('report.search');
Route::post('system-management/report/excel', 'ReportController@exportExcel')->name('report.excel');
Route::post('system-management/report/pdf', 'ReportController@exportPDF')->name('report.pdf');

Route::get('avatars/{name}', 'EmployeeManagementController@load');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/realtime-ticketing', 'HomeController@live')->name('live');

Route::get('ajax/on-progress', 'HomeController@progress');
Route::get('ajax/emergencys', 'HomeController@emergency');
Route::get('ajax/over-deadline', 'HomeController@deadline');
Route::get('ajax/finish', 'HomeController@finish');
