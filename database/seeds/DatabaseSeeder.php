<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $user = factory(App\User::class)->create([
             'username' => 'admin',
             'email' => 'ilyas.raswandi@gmail.com',
             'password' => bcrypt('admin'),
             'lastname' => 'Mr',
             'firstname' => 'admin',
             'roles'=>0,
             'employee_id'=>0,
			 'created_by' => '1',
			 'updated_by' => '0',
			 'deleted_by' => '0'
         ]);
    }
}
