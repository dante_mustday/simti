<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticketing_code');
			$table->string('machine_code');
			$table->string('serial_number')->nullable();
			$table->string('machine')->nullable();
            $table->integer('is_wo')->default(0);
            $table->integer('is_internal')->default(0);
			$table->integer('customer_id');
			$table->string('technician_id');
            $table->string('city_id');
			$table->string('machine_location');
            $table->string('call_name');
            $table->string('call_phone');
            $table->time('call_time');
            $table->time('sla_id')->default(0);
			$table->date('open_ticket_date');
			$table->datetime('closed_ticket_date')->nullable();
			$table->integer('created_by')->default(1);
			$table->integer('updated_by')->default(0);
			$table->integer('deleted_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing');
    }
}
