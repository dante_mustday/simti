<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersCoverageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('partners_coverage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id');
            $table->integer('city_id');
            $table->integer('price_install');
            $table->integer('price_service');
            $table->integer('price_maintenance');
            $table->integer('price_toner');
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(0);
            $table->integer('deleted_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_coverage');
    }
}
