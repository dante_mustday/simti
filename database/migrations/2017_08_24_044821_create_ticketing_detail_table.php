<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticketing_code');
			$table->date('visit_date');
			$table->time('start');
			$table->time('finish');
            $table->string('part_number');
			$table->text('description');
			$table->string('action');
			$table->string('result');
			$table->integer('color_meter');
			$table->integer('mono_meter');
            $table->integer('total_meter');
			$table->string('technician_id');
			$table->string('technician_name');
            $table->integer('change_part_approval')->default(0);
            $table->integer('supervisor_approval')->default(0);
			$table->integer('created_by')->default(1);
			$table->integer('updated_by')->default(0);
			$table->integer('deleted_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_detail');
    }
}
