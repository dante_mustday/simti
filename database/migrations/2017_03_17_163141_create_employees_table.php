<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastname', 60);
            $table->string('firstname', 60);
            $table->string('middlename', 60);
            $table->string('address', 120);
            $table->integer('city_id')->default(0);
            $table->integer('state_id')->default(0);
            $table->integer('country_id')->default(0);
            $table->char('zip', 10)->nullable();
            $table->integer('age')->nullable();
            $table->date('birthdate')->nullable();
            $table->date('date_hired')->nullable();
            $table->string('position')->default(0);
            $table->integer('department_id')->default(0);
            $table->integer('division_id')->default(0);
            $table->integer('company_id')->default(0);
            $table->string('picture', 60);
			$table->integer('created_by')->default(1);
			$table->integer('updated_by')->default(0);
			$table->integer('deleted_by')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
