<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketingRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_request_sparepart', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticketing_code');
            $table->string('machine_code');
            $table->string('sparepart_code');
            $table->string('sparepart_name');
            $table->integer('is_approved')->default(0);
            $table->string('status');
            $table->integer('supervisor_id');
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(0);
            $table->integer('deleted_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_request_sparepart');
    }
}
