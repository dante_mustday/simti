<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contract_number');
			$table->string('machine_code');
            $table->string('ask_code');
			$table->string('serial_number');
			$table->string('machine');
			$table->string('location');
			$table->string('users');
            $table->integer('color_meter');
            $table->integer('mono_meter');
            $table->integer('total_meter');
			$table->integer('created_by')->default(1);
			$table->integer('updated_by')->default(0);
			$table->integer('deleted_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_detail');
    }
}
